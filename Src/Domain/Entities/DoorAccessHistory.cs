﻿using Domain.Common;

namespace Domain.Entities
{
    /// <summary>
    /// The history of user access to doors
    /// </summary>
    public class DoorAccessHistory : BaseDomain
    {
        private DoorAccessHistory() { }

        public DoorAccessHistory(int userId, int doorId)
        {
            UserId = userId;
            DoorId = doorId;
        }

        public int DoorId { get; private set; }
        public Door Door { get; private set; }

        public int UserId { get; private set; }
        public User User { get; private set; }
    }
}

﻿using Domain.Common;

namespace Domain.Entities
{
    /// <summary>
    /// The doors info
    /// </summary>
    public class Door : BaseDomain
    {
        private Door() { }

        public Door(string name, string externalId)
        {
            Name = name;
            ExternalId = externalId;
        }

        public string Name { get; private set; }
        public string ExternalId { get; private set; }
    }
}

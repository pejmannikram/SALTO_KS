﻿using Domain.Common;
using Domain.Enums;

namespace Domain.Entities
{
    /// <summary>
    /// Each user can have 1 to N roles
    /// </summary>
    public class UserRole : BaseDomain
    {
        private UserRole() { }

        /// <summary>
        /// use only in database seeder
        /// </summary>  
        public UserRole(int id, UserRoleType roleId, int userId) : base(id)
        {
            RoleId = roleId;
            UserId = userId;
        }

        public UserRole(UserRoleType roleId, User user)
        {
            RoleId = roleId;
            User = user;
        }

        public UserRole(UserRoleType roleId, int userId)
        {
            RoleId = roleId;
            UserId = userId;
        }

        public UserRoleType RoleId { get; private set; }

        public int UserId { get; private set; }
        public User User { get; private set; }
    }
}

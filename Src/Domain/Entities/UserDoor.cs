﻿using Domain.Common;

namespace Domain.Entities
{
    /// <summary>
    /// Each user can access to 0:N doors
    /// </summary>
    public class UserDoor : BaseDomain
    {
        private UserDoor() { }

        public UserDoor(int userId, int doorId)
        {
            UserId = userId;
            DoorId = doorId;
        }

        public int DoorId { get; private set; }
        public Door Door { get; private set; }

        public int UserId { get; private set; }
        public User User { get; private set; }
    }
}

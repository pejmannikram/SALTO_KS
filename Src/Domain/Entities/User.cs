﻿using Domain.Common;
using System.Collections.Generic;

namespace Domain.Entities
{
    public class User : BaseDomain
    {
        private User() { }

        public User(string firstName, string lastName, string email)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            Password = "";
        }

        /// <summary>
        /// use only in database seeder
        /// </summary>      
        public User(int id, string firstName, string lastName, string email, string password) : base(id)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            Password = password;
        }

        public void SetPassword(string password)
        {
            Password = password;
        }

        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string Email { get; private set; }
        public string Password { get; private set; }

        public IEnumerable<UserRole> UserRoles { get; private set; }
    }
}

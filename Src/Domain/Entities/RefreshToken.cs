﻿using Domain.Common;
using System;

namespace Domain.Entities
{
    //TODO: add a background job to remove expired refresh token
    public class RefreshToken : BaseDomain
    {
        private const int validDays = 20;

        private RefreshToken() { }

        public RefreshToken(int userId)
        {
            UserId = userId;
        }

        public int UserId { get; private set; }
        public User User { get; private set; }

        public string Token { get; private set; }
        public DateTime ExpirationDate { get; private set; }

        public void UpdateToken(string token, DateTime now)
        {
            Token = token;
            ExpirationDate = now.AddDays(validDays);
        }
    }
}

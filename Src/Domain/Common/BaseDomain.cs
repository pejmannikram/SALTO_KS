﻿using System;

namespace Domain.Common
{
    public class BaseDomain
    {
        public BaseDomain() { }

        public BaseDomain(int id)
        {
            Id = id;
        }

        public int Id { get; private set; }
        public DateTime CreatedDate { get; set; }
    }
}

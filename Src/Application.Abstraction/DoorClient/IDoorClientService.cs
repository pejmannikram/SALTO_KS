﻿using System.Threading.Tasks;

namespace Application.Abstraction.DoorClient
{
    public interface IDoorClientService
    {
        Task Open(string doorExternalId);
    }
}

﻿using Domain.Entities;
using System;

namespace Application.Abstraction.Doors.Models
{
    public class DoorVM
    {
        public DoorVM(Door door)
        {
            Id = door.Id;
            CreatedDate = door.CreatedDate;
            Name = door.Name;
            ExternalId = door.ExternalId;
        }

        public int Id { get; set; }
        public DateTime CreatedDate { get; set; }

        public string Name { get; private set; }
        public string ExternalId { get; private set; }
    }
}

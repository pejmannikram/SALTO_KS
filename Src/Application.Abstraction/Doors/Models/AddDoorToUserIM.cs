﻿namespace Application.Abstraction.Doors.Models
{
    public class AddDoorToUserIM
    {
        public AddDoorToUserIM(int doorId)
        {
            DoorId = doorId;
        }

        public int DoorId { get; }
    }
}

﻿using Domain.Entities;
using System;
using System.Linq.Expressions;

namespace Application.Abstraction.Doors.Models
{
    public class DoorAccessHistoryVM
    {
        private DoorAccessHistoryVM()
        {
        }

        public static Expression<Func<DoorAccessHistory, DoorAccessHistoryVM>> Expression()
        {
            return x => new DoorAccessHistoryVM
            {
                Id = x.Id,
                CreatedDate = x.CreatedDate,
                UserId = x.UserId,
                DoorId = x.DoorId,
                Door = new DoorVM(x.Door.Name),
                User = new UserVM(x.User.FirstName, x.User.LastName),
            };
        }

        public int Id { get; init; }
        public DateTime CreatedDate { get; init; }
        public int UserId { get; init; }
        public int DoorId { get; init; }

        public UserVM? User { get; init; }
        public DoorVM? Door { get; init; }

        public record DoorVM(string Name) { }

        public record UserVM(string FirstName, string LastName) { }
    }
}

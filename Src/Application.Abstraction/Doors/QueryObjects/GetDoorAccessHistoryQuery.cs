﻿using Domain.Entities;
using EFQueryObject;

namespace Application.Abstraction.Doors.QueryObjects
{
    public class GetDoorAccessHistoryQuery : IQueryObject<DoorAccessHistory>
    {
        public GetDoorAccessHistoryQuery(int doorId)
        {
            DoorId = doorId;
        }

        public int DoorId { get; }
    }
}

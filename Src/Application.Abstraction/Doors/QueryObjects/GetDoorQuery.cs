﻿using Domain.Entities;
using EFQueryObject;

namespace Application.Abstraction.Doors.QueryObjects
{
    public class GetDoorQuery : IQueryObject<Door>
    {
    }
}

﻿using Domain.Entities;
using EFQueryObject;

namespace Application.Abstraction.Doors.QueryObjects
{
    public class GetDoorByUserQuery : IQueryObject<Door>
    {
        public GetDoorByUserQuery(int userId, int doorId)
        {
            UserId = userId;
            DoorId = doorId;
        }

        public int UserId { get; }
        public int DoorId { get; }
    }
}

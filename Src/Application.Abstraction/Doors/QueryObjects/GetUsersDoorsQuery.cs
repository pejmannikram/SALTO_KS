﻿using Domain.Entities;
using EFQueryObject;

namespace Application.Abstraction.Doors.QueryObjects
{
    public class GetUsersDoorsQuery : IQueryObject<UserDoor>
    {
        public GetUsersDoorsQuery(int userId)
        {
            UserId = userId;
        }

        public int UserId { get; }
    }
}

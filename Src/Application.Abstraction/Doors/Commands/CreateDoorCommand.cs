﻿using CommandR;
using Domain.Entities;

namespace Application.Abstraction.Doors.Commands
{
    public class CreateDoorCommand : ICommand<Door>
    {
        /// <summary>
        /// support path model
        /// </summary>
        private CreateDoorCommand()
        {
            Name = "";
            ExternalId = "";
        }

        public CreateDoorCommand(string name, string externalId)
        {
            Name = name;
            ExternalId = externalId;
        }

        public string Name { get; private set; }
        public string ExternalId { get; private set; }
    }
}

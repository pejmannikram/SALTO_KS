﻿using CommandR;
using Domain.Entities;

namespace Application.Abstraction.Doors.Commands
{
    public class DeleteDoorCommand : ICommand<Door>
    {
        public DeleteDoorCommand(int id)
        {
            Id = id;
        }

        public int Id { get; private set; }
    }
}

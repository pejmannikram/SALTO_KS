﻿using CommandR;

namespace Application.Abstraction.Doors.Commands
{
    public class OpenDoorCommand : ICommand<VoidResult>
    {
        public OpenDoorCommand(int id)
        {
            Id = id;
        }

        public int Id { get; private set; }
    }
}

﻿using CommandR;
using Domain.Entities;

namespace Application.Abstraction.Doors.Commands
{
    public class DeleteUsersDoorAccessCommand : ICommand<UserDoor>
    {
        public DeleteUsersDoorAccessCommand(int userId, int doorId)
        {
            UserId = userId;
            DoorId = doorId;
        }

        public int DoorId { get; private set; }
        public int UserId { get; private set; }
    }
}

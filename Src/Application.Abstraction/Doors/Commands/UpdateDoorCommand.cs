﻿using ApiPatchModel;
using CommandR;
using Domain.Entities;

namespace Application.Abstraction.Doors.Commands
{
    public class UpdateDoorCommand : ICommand<Door>
    {
        public UpdateDoorCommand(int id, PatchModel<CreateDoorCommand> patchModel)
        {
            Id = id;
            PatchModel = patchModel;
        }

        public int Id { get; private set; }
        public PatchModel<CreateDoorCommand> PatchModel { get; private set; }
    }
}

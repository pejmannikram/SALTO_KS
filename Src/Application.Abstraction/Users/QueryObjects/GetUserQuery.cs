﻿using Domain.Entities;
using EFQueryObject;

namespace Application.Abstraction.Users.QueryObjects
{
    public class GetUserQuery : IQueryObject<User>
    {
    }
}

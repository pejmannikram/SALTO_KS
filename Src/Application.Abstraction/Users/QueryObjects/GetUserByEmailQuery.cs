﻿using Domain.Entities;
using EFQueryObject;

namespace Application.Abstraction.Users.QueryObjects
{
    public class GetUserByEmailQuery : IQueryObject<User>
    {
        public GetUserByEmailQuery(string email)
        {
            Email = email;
        }

        public string Email { get; }
    }
}

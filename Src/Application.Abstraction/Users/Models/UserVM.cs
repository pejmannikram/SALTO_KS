﻿using Domain.Entities;
using System;

namespace Application.Abstraction.Users.Models
{
    public class UserVM
    {
        public UserVM(User user)
        {
            Id = user.Id;
            FirstName = user.FirstName;
            LastName = user.LastName;
            Email = user.Email;
            Password = "******";
            CreatedDate = user.CreatedDate;
        }

        public int Id { get; set; }
        public DateTime CreatedDate { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}

﻿namespace Application.Abstraction.Users.Models
{
    public class UpdateProfileIM
    {
        /// <summary>
        /// support path model
        /// </summary>
        private UpdateProfileIM()
        {
            FirstName = "";
            LastName = "";
        }

        public string FirstName { get; private set; }
        public string LastName { get; private set; }
    }
}

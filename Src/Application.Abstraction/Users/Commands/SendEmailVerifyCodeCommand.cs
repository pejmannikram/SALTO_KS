﻿using CommandR;

namespace Application.Abstraction.Users.Commands
{
    public class SendEmailVerifyCodeCommand : ICommand<VoidResult>
    {
        public SendEmailVerifyCodeCommand(string email)
        {
            Email = email;
        }

        public string Email { get; private set; }
    }
}

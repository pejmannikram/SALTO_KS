﻿using ApiPatchModel;
using Application.Abstraction.Users.Models;
using CommandR;
using Domain.Entities;

namespace Application.Abstraction.Users.Commands
{
    public class UpdateProfileCommand : ICommand<User>
    {
        public UpdateProfileCommand(PatchModel<UpdateProfileIM> patchModel)
        {
            PatchModel = patchModel;
        }

        public PatchModel<UpdateProfileIM> PatchModel { get; private set; }
    }
}

﻿using CommandR;
using Domain.Entities;

namespace Application.Abstraction.Users.Commands
{
    public class DeleteUserCommand : ICommand<User>
    {
        public DeleteUserCommand(int id)
        {
            Id = id;
        }

        public int Id { get; private set; }
    }
}

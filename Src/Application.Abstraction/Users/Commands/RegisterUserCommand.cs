﻿using Application.Abstraction.Auth.Models;
using CommandR;

namespace Application.Abstraction.Users.Commands
{
    public class RegisterUserCommand : ICommand<AuthTokenVM>
    {
        public RegisterUserCommand(string firstName, string lastName, string email, string password, string verifyCode)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            Password = password;
            VerifyCode = verifyCode;
        }

        public string FirstName { get; }
        public string LastName { get; }
        public string Email { get; }

        public string Password { get; }
        public string VerifyCode { get; }
    }
}

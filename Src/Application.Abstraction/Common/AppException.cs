﻿using System;
using System.Linq.Expressions;
using System.Net;

namespace Application.Abstraction.Common
{
    public class AppException : Exception
    {
        public AppException(Expression<Func<AppExceptionProvider, string>> error, HttpStatusCode statusCode) : base(GetErrorMessage(error))
        {
            StatusCode = statusCode;
        }

        public HttpStatusCode StatusCode { get; }

        private static string GetErrorMessage(Expression<Func<AppExceptionProvider, string>> error)
        {
            //TODO: we can use error title as code
            return error.Compile()(new AppExceptionProvider());
        }
    }
}

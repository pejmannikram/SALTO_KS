﻿namespace Application.Abstraction.Common
{
    public class AppExceptionProvider
    {
        public string WrongCredential() => "The username and/or password is wrong";
        public string WrongRefreshToken() => "The refresh token is wrong";
        public string NotExist(string name) => $"The {name} is not exist";
        public string NotExist() => $"The recourse is not exist";
        public string IsDuplicate(string name) => $"The {name} is duplicate";
        public string UserNeedsOneRoleAtLeast() => $"User needs one role at least";
        public string CanNotRemoveYourSelf() => $"You can not remove your self";
        public string TokenIsNotValid() => $"The token is not valid";
    }
}

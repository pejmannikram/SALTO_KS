﻿using Domain.Enums;

namespace Application.Abstraction.Common
{
    public interface IUserService
    {
        int UserId { get; }
        bool IsInRole(params UserRoleType[] userRoles);
    }
}

﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Abstraction.Common
{
    public interface IAPIDbContext
    {
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
        Task<int> ExecuteSqlRaw(string sql, params object[] parameters);
        Task<int> ExecuteSqlRaw(string sql);
        Task<IDbContextTransaction> BeginTransaction();

        DbSet<User> Users { get; }
        DbSet<Door> Doors { get; }
        DbSet<UserDoor> UserDoors { get; }
        DbSet<DoorAccessHistory> DoorAccessHistories { get; }
        DbSet<UserRole> UserRoles { get; }
        DbSet<RefreshToken> RefreshTokens { get; }
    }
}

﻿using Domain.Entities;
using EFQueryObject;

namespace Application.Abstraction.UsersRoles.QueryObjects
{
    public class GetUsersRolesQuery : IQueryObject<UserRole>
    {
        public GetUsersRolesQuery(int userId)
        {
            UserId = userId;
        }

        public int UserId { get; private set; }
    }
}

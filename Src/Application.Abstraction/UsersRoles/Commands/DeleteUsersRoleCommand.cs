﻿using CommandR;
using Domain.Entities;
using Domain.Enums;

namespace Application.Abstraction.UsersRoles.Commands
{
    public class DeleteUsersRoleCommand : ICommand<UserRole>
    {
        public DeleteUsersRoleCommand(int userId, UserRoleType roleId)
        {
            UserId = userId;
            RoleId = roleId;
        }

        public UserRoleType RoleId { get; private set; }
        public int UserId { get; private set; }
    }
}

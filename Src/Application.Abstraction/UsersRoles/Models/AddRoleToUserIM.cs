﻿using Domain.Enums;

namespace Application.Abstraction.UsersRoles.Models
{
    public class AddRoleToUserIM
    {
        public AddRoleToUserIM(UserRoleType roleId)
        {
            RoleId = roleId;
        }

        public UserRoleType RoleId { get; }
    }
}

﻿using Domain.Entities;
using System;

namespace Application.Abstraction.UsersRoles.Models
{
    public class UserRoleVM
    {
        public UserRoleVM(UserRole userRole)
        {
            RoleId = (int)userRole.RoleId;
            Role = Enum.GetName(userRole.RoleId) ?? "";
            UserId = userRole.UserId;
        }

        public int RoleId { get; private set; }
        public string Role { get; private set; }

        public int UserId { get; private set; }
    }
}

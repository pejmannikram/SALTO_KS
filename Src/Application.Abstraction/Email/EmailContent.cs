﻿using System.Collections.Generic;

namespace Application.Abstraction.Email
{
    public class EmailContent
    {
        public EmailContent(string subject, string content, Dictionary<string, string>? parameters = null)
        {
            Subject = subject;
            Content = content;
            Parameters = parameters ?? new Dictionary<string, string>();
        }

        public Dictionary<string, string> Parameters { get; }
        public string Subject { get; }
        public string Content { get; }
    }
}

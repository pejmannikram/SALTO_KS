﻿using System.Threading.Tasks;

namespace Application.Abstraction.Email
{
    public interface IEmailService
    {
        Task Send(string to, EmailContent content);
    }
}

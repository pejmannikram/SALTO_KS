﻿namespace Application.Abstraction.Auth.Models
{
    public enum GrantType
    {
        Password,
        RefreshToken,
    }
}

﻿namespace Application.Abstraction.Auth.Models
{
    public class TokenIM
    {

        public TokenIM(string? username, string? password, string? refreshToken, GrantType grantType)
        {
            Username = username;
            Password = password;
            RefreshToken = refreshToken;
            GrantType = grantType;
        }

        public string? Username { get; set; }
        public string? Password { get; set; }
        public string? RefreshToken { get; set; }
        public GrantType GrantType { get; set; }
    }
}

﻿using System;

namespace Application.Abstraction.Auth.Models
{
    public class AuthTokenVM
    {
        public AuthTokenVM(string accessToken, string refreshToken, DateTime expiresIn)
        {
            AccessToken = accessToken;
            ExpiresIn = expiresIn;
            RefreshToken = refreshToken;
        }

        public string AccessToken { get; }
        public string TokenType { get; } = "bearer";
        public string RefreshToken { get; private set; }
        public DateTime ExpiresIn { get; }

        public void SetRefreshTokenId(int id)
        {
            this.RefreshToken = id.ToString() + "_" + RefreshToken;
        }
    }
}

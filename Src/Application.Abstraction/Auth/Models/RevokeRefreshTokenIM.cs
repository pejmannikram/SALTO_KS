﻿namespace Application.Abstraction.Auth.Models
{
    public class RevokeRefreshTokenIM
    {
        public RevokeRefreshTokenIM(string refreshToken)
        {
            RefreshToken = refreshToken;
        }

        public string RefreshToken { get; }
    }
}

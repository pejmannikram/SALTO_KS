﻿namespace Application.Abstraction.Auth.Commands
{
    public record RefreshTokenRecord(string token, int id)
    {

        public static implicit operator RefreshTokenRecord(string refreshToken)
        {
            var parts = refreshToken.Split("_");
            var token = parts[1];
            _ = int.TryParse(parts[0], out int id);

            return new RefreshTokenRecord(token, id);
        }
    }
}

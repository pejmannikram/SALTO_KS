﻿using Application.Abstraction.Auth.Models;
using CommandR;

namespace Application.Abstraction.Auth.Commands
{
    public class LoginCommand : ICommand<AuthTokenVM>
    {
        public LoginCommand(string email, string password)
        {
            Email = email;
            Password = password;
        }

        public string Email { get; }
        public string Password { get; }
    }
}

﻿using Application.Abstraction.Auth.Models;
using CommandR;

namespace Application.Abstraction.Auth.Commands
{
    public class RefreshAuthTokenCommand : ICommand<AuthTokenVM>
    {
        public RefreshAuthTokenCommand(string refreshToken)
        {
            RefreshToken = refreshToken;
        }

        public RefreshTokenRecord RefreshToken { get; }
    }
}

﻿using CommandR;

namespace Application.Abstraction.Auth.Commands
{
    public class RevokeRefreshTokenCommand : ICommand<VoidResult>
    {
        public RevokeRefreshTokenCommand(string refreshToken)
        {
            RefreshToken = refreshToken;
        }

        public RefreshTokenRecord RefreshToken { get; }
    }
}

﻿using Domain.Entities;
using EFQueryObject;

namespace Application.Abstraction.Auth.QueryObjects
{
    public class GetUserRefreshTokenQuery : IQueryObject<RefreshToken>
    {
        public GetUserRefreshTokenQuery(int refreshTokenId)
        {
            RefreshTokenId = refreshTokenId;
        }

        public int RefreshTokenId { get; }
    }
}

﻿using ApiPatchModel;
using Application.Abstraction.Doors.Commands;
using Application.Abstraction.Doors.Models;
using Application.Abstraction.Doors.QueryObjects;
using CommandR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sieve.Models;
using System.Linq;
using System.Threading.Tasks;
using Web.Common;
using Web.Services;

namespace Web.Controllers
{
    /// <summary>
    /// Admin: Manage/View doors info
    /// User: Open/View doors that she/he has access to it.
    /// </summary>
    [Route("api/doors")]
    public class DoorController : Controller
    {
        private readonly IMediator mediator;
        private readonly IQueryResponseService queryResponse;

        public DoorController(IMediator mediator, IQueryResponseService queryResponse)
        {
            this.mediator = mediator;
            this.queryResponse = queryResponse;
        }

        [Authorize(Roles = ApiUserRole.User)]
        [HttpPost("{id}/open")]
        public async Task<IActionResult> Open(int id)
        {
            await mediator.Send(new OpenDoorCommand(id));
            return new OkApiResponse();
        }

        [HttpGet]
        public async Task<IActionResult> GetList(SieveModel sieveModel)
        {
            var query = await mediator.Send(new GetDoorQuery());
            return await queryResponse
                 .From(query)
                 .List<DoorVM>(sieveModel);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var query = await mediator.Send(new GetDoorQuery());
            return await queryResponse
                 .From(query.Where(x => x.Id == id))
                 .Single<DoorVM>();
        }

        [Authorize(Roles = ApiUserRole.Admin)]
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CreateDoorCommand command)
        {
            var door = await mediator.Send(command);
            return new CreationApiResponse(door.Id);
        }

        [Authorize(Roles = ApiUserRole.Admin)]
        [HttpPatch("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] PatchModel<CreateDoorCommand> model)
        {
            await mediator.Send(new UpdateDoorCommand(id, model));
            return new OkApiResponse();
        }

        [Authorize(Roles = ApiUserRole.Admin)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await mediator.Send(new DeleteDoorCommand(id));
            return new OkApiResponse();
        }
    }
}

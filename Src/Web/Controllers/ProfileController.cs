﻿using ApiPatchModel;
using Application.Abstraction.Common;
using Application.Abstraction.Users.Commands;
using Application.Abstraction.Users.Models;
using Application.Abstraction.Users.QueryObjects;
using CommandR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Web.Common;
using Web.Services;

namespace Web.Controllers
{
    /// <summary>
    /// View/Update current user profile 
    /// </summary>

    [Authorize]
    [Route("api/profile")]
    public class ProfileController : Controller
    {
        private readonly IMediator mediator;
        private readonly IUserService userService;
        private readonly IQueryResponseService queryResponse;

        public ProfileController(
            IMediator mediator,
            IUserService userService,
            IQueryResponseService queryResponse)
        {
            this.mediator = mediator;
            this.userService = userService;
            this.queryResponse = queryResponse;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var query = await mediator.Send(new GetUserQuery());
            return await queryResponse
                .From(query.Where(x => x.Id == userService.UserId))
                .Single<UserVM>();
        }

        [HttpPatch()]
        public async Task<IActionResult> Update([FromBody] PatchModel<UpdateProfileIM> model)
        {
            await mediator.Send(new UpdateProfileCommand(model));
            return new OkApiResponse();
        }

        //TODO: Change Email
        //TODO: Change Password
        //TODO: Reset Password
    }
}

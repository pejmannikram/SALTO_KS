﻿using Application.Abstraction.Users.Commands;
using Application.Abstraction.Users.Models;
using Application.Abstraction.Users.QueryObjects;
using CommandR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sieve.Models;
using System.Linq;
using System.Threading.Tasks;
using Web.Common;
using Web.Services;

namespace Web.Controllers
{
    [Authorize(Roles = ApiUserRole.Admin)]
    [Route("api/users")]
    public class UserController : Controller
    {
        private readonly IMediator mediator;
        private readonly IQueryResponseService queryResponse;

        public UserController(
            IMediator mediator,
            IQueryResponseService queryResponse)
        {
            this.mediator = mediator;
            this.queryResponse = queryResponse;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var query = await mediator.Send(new GetUserQuery());
            return await queryResponse
                .From(query.Where(x => x.Id == id))
                .Single<UserVM>();
        }

        [HttpGet]
        public async Task<IActionResult> GetList(SieveModel sieveModel)
        {
            var query = await mediator.Send(new GetUserQuery());
            return await queryResponse
               .From(query)
               .List<UserVM>(sieveModel);
        }


        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await mediator.Send(new DeleteUserCommand(id));
            return new OkApiResponse();
        }
    }
}

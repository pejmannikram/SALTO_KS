﻿using Application.Abstraction.Auth.Commands;
using Application.Abstraction.Auth.Models;
using CommandR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Web.Common;

namespace Web.Controllers
{
    /// <summary>
    /// Implement Password Grant type of OAuth
    /// </summary>
    [Route("api/auth")]
    public class AuthController : Controller
    {
        private readonly IMediator mediator;

        public AuthController(
           IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost("token")]
        public async Task<IActionResult> Login([FromBody] TokenIM model)
        {
            AuthTokenVM result;

            if (model.GrantType == GrantType.Password)
            {
                result = await mediator.Send(new LoginCommand(model.Username!, model.Password!));
            }
            else
            {
                result = await mediator.Send(new RefreshAuthTokenCommand(model.RefreshToken!));
            }

            Request.HttpContext.Response.Headers.Add("Cache-Control", "no-store");
            Request.HttpContext.Response.Headers.Add("Pragma", "no-cache");

            return new OkApiResponse(result);
        }

        /// <summary>
        /// Revoke a refresh token
        /// This method always returns success
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost("revoke")]
        public async Task<IActionResult> Login([FromBody] RevokeRefreshTokenIM model)
        {
            await mediator.Send(new RevokeRefreshTokenCommand(model.RefreshToken));
            return new OkApiResponse();
        }

    }
}

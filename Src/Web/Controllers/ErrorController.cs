﻿using Application.Abstraction.Common;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using Web.Common;

namespace Web.Controllers
{
    [Route("/error")]
    public class ErrorController : Controller
    {
        public IActionResult Error()
        {
            var exception = HttpContext.Features.Get<IExceptionHandlerPathFeature>();
            if (exception.Error is UnauthorizedAccessException)
            {
                return new ErrorApiResponse(HttpStatusCode.Unauthorized,
                    new ApiError[] { new ApiError("You are not Unauthorized to do this action.") });
            }
            else if (exception.Error is AppException appException)
            {
                return new ErrorApiResponse(appException.StatusCode,
                    new ApiError[] { new ApiError(appException.Message) });
            }


            return new ErrorApiResponse(HttpStatusCode.InternalServerError,
                new ApiError[] {
                    new ApiError("An error has occurred. Please review your request and try again later")
                });
        }
    }
}

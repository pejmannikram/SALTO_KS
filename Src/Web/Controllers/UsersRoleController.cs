﻿using Application.Abstraction.UsersRoles.Commands;
using Application.Abstraction.UsersRoles.Models;
using Application.Abstraction.UsersRoles.QueryObjects;
using CommandR;
using Domain.Enums;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sieve.Models;
using System.Linq;
using System.Threading.Tasks;
using Web.Common;
using Web.Services;

namespace Web.Controllers
{
    /// <summary>
    /// Manage/View User roles by admin
    /// The role is an internal enum and we don't have roles table
    /// </summary>
    [Authorize(Roles = ApiUserRole.Admin)]
    [Route("api/users/{userId}/roles/")]
    public class UsersRoleController : Controller
    {
        private readonly IMediator mediator;
        private readonly IQueryResponseService queryResponse;

        public UsersRoleController(
            IMediator mediator,
            IQueryResponseService queryResponse)
        {
            this.mediator = mediator;
            this.queryResponse = queryResponse;
        }

        [HttpGet]
        public async Task<IActionResult> GetList(int userId, SieveModel sieveModel)
        {
            var query = await mediator.Send(new GetUsersRolesQuery(userId));
            return await queryResponse
                 .From(query)
                 .List<UserRoleVM>(sieveModel);
        }

        [HttpGet("{roleId}")]
        public async Task<IActionResult> Get(int userId, int roleId)
        {
            var query = await mediator.Send(new GetUsersRolesQuery(userId));
            return await queryResponse
                 .From(query.Where(x => x.Id == roleId))
                 .Single<UserRoleVM>();
        }

        [HttpPost]
        public async Task<IActionResult> AddRole(int userId, [FromBody] AddRoleToUserIM model)
        {
            var userRole = await mediator.Send(new AddRoleToUserCommand(userId, model.RoleId));
            return new CreationApiResponse(userRole.Id);
        }

        [HttpDelete("{roleId}")]
        public async Task<IActionResult> DeleteRole(int userId, int roleId)
        {
            await mediator.Send(new DeleteUsersRoleCommand(userId, (UserRoleType)roleId));
            return new OkApiResponse();
        }
    }
}

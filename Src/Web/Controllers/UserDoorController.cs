﻿using Application.Abstraction.Doors.Commands;
using Application.Abstraction.Doors.Models;
using Application.Abstraction.Doors.QueryObjects;
using CommandR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sieve.Models;
using System.Linq;
using System.Threading.Tasks;
using Web.Common;
using Web.Services;

namespace Web.Controllers
{
    /// <summary>
    /// Manage/View user's access to doors by admin
    /// </summary>
    [Authorize(Roles = ApiUserRole.Admin)]
    [Route("api/users/{userId}/doors/")]
    public class UserDoorController : Controller
    {
        private readonly IMediator mediator;
        private readonly IQueryResponseService queryResponse;

        public UserDoorController(
            IMediator mediator,
            IQueryResponseService queryResponse)
        {
            this.mediator = mediator;
            this.queryResponse = queryResponse;
        }

        [HttpGet]
        public async Task<IActionResult> GetList(int userId, SieveModel sieveModel)
        {
            var query = await mediator.Send(new GetUsersDoorsQuery(userId));
            return await queryResponse
                .From(query.Select(x => x.Door))
                .List<DoorVM>(sieveModel);
        }

        [HttpGet("{doorId}")]
        public async Task<IActionResult> Get(int userId, int doorId)
        {
            var query = await mediator.Send(new GetUsersDoorsQuery(userId));
            return await queryResponse
                .From(query.Where(x => x.Id == doorId).Select(x => x.Door))
                .Single<DoorVM>();
        }

        [HttpPost]
        public async Task<IActionResult> AddDoorAccess(int userId, [FromBody] AddDoorToUserIM model)
        {
            await mediator.Send(new AddDoorAccessToUserCommand(userId, model.DoorId));
            return new OkApiResponse();
        }

        [HttpDelete("{doorId}")]
        public async Task<IActionResult> DeleteDoorAccess(int userId, int doorId)
        {
            await mediator.Send(new DeleteUsersDoorAccessCommand(userId, doorId));
            return new OkApiResponse();
        }

        // TODO: Add HttpPut - accept array of (userId,doorId)
        // for batch operation on a user's Access
    }
}

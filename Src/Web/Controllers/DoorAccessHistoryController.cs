﻿using Application.Abstraction.Doors.Models;
using Application.Abstraction.Doors.QueryObjects;
using CommandR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sieve.Models;
using System.Linq;
using System.Threading.Tasks;
using Web.Common;
using Web.Services;

namespace Web.Controllers
{
    /// <summary>
    /// View doors access history info by admin
    /// </summary>
    [Authorize(Roles = ApiUserRole.Admin)]
    [Route("api/doors/{doorId}/access-histories")]
    public class DoorAccessHistoryController : Controller
    {
        private readonly IMediator mediator;
        private readonly IQueryResponseService queryResponse;

        public DoorAccessHistoryController(IMediator mediator, IQueryResponseService queryResponse)
        {
            this.mediator = mediator;
            this.queryResponse = queryResponse;
        }

        [HttpGet]
        public async Task<IActionResult> GetList(int doorId, SieveModel sieveModel)
        {
            var query = await mediator.Send(new GetDoorAccessHistoryQuery(doorId));
            return await queryResponse
                 .From(query)
                 .List(sieveModel, DoorAccessHistoryVM.Expression());
        }

        [HttpGet("{accessHistoryId}")]
        public async Task<IActionResult> Get(int doorId, int accessHistoryId)
        {
            var query = await mediator.Send(new GetDoorAccessHistoryQuery(doorId));
            return await queryResponse
                 .From(query.Where(x => x.Id == accessHistoryId))
                 .Single(DoorAccessHistoryVM.Expression());
        }
    }
}

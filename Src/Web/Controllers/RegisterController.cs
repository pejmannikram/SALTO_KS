﻿using Application.Abstraction.Users.Commands;
using CommandR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Web.Common;

namespace Web.Controllers
{
    [Route("api/register")]
    public class RegisterController : Controller
    {
        private readonly IMediator mediator;

        public RegisterController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpPost("verify-code")]
        public async Task<IActionResult> VerifyEmail([FromBody] SendEmailVerifyCodeCommand model)
        {
            await mediator.Send(model);
            return new OkApiResponse();
        }

        [HttpPost]
        public async Task<IActionResult> Register([FromBody] RegisterUserCommand model)
        {
            var token = await mediator.Send(model);
            return new OkApiResponse(token);
        }
    }
}

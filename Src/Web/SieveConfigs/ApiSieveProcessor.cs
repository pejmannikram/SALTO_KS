﻿using Domain.Entities;
using Microsoft.Extensions.Options;
using Sieve.Models;
using Sieve.Services;

namespace Web.SieveConfigs
{
    public class ApiSieveProcessor : SieveProcessor
    {
        public ApiSieveProcessor(
            IOptions<SieveOptions> options)
            : base(options)
        {
        }

        protected override SievePropertyMapper MapProperties(SievePropertyMapper mapper)
        {
            mapper.Property<User>(x => x.Id)
                .CanFilter()
                .CanSort();

            mapper.Property<User>(x => x.Email)
               .CanFilter()
               .CanSort();

            mapper.Property<User>(x => x.FirstName)
              .CanFilter()
              .CanSort();

            mapper.Property<User>(x => x.LastName)
              .CanFilter()
              .CanSort();

            mapper.AllPropertiesOf<Door>();
            mapper.AllPropertiesOf<UserRole>();
            mapper.AllPropertiesOf<DoorAccessHistory>();

            return mapper;
        }
    }
}

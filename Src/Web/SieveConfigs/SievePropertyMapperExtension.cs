﻿using Sieve.Services;

namespace Web.SieveConfigs
{
    public static class SievePropertyMapperExtension
    {
        public static void AllPropertiesOf<T>(this SievePropertyMapper mapper)
        {
            var properties = typeof(T).GetProperties();
            foreach (var property in properties)
            {
                mapper.FindProperty<T>(true, true, property.Name, false);
            }
        }
    }
}

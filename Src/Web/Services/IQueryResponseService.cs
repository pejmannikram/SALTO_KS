﻿using System.Linq;

namespace Web.Services
{
    public interface IQueryResponseService
    {
        QueryResponse<TDomain> From<TDomain>(IQueryable<TDomain> query) where TDomain : class;
    }
}

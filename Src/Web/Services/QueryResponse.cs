﻿using Application.Abstraction.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Sieve.Models;
using Sieve.Services;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using Web.Common;

namespace Web.Services
{
    /// <summary>
    /// Get a query object class and return APIResponse from that query
    /// </summary>
    /// <typeparam name="TDomain"></typeparam>
    public class QueryResponse<TDomain> where TDomain : class
    {
        private readonly IQueryable<TDomain> query;
        private readonly ISieveProcessor sieveProcessor;
        private readonly SieveOptions sieveOptions;

        public QueryResponse(
            IQueryable<TDomain> query,
            ISieveProcessor sieveProcessor,
            IOptions<SieveOptions> sieveOptions)
        {
            this.query = query;
            this.sieveProcessor = sieveProcessor;
            this.sieveOptions = sieveOptions.Value;
        }

        /// <summary>
        /// Add projection to query and get first record
        /// </summary>
        /// <typeparam name="TViewModel"></typeparam>
        /// <param name="select">custom select expression</param>
        /// <returns></returns>
        public async Task<ApiResponse> Single<TViewModel>(Expression<Func<TDomain, TViewModel>>? select = null)
        {
            var data = await query
                .AsNoTracking()
                .Select(select ?? GetSelectExpression<TViewModel>())
                .FirstOrDefaultAsync();

            if (data is null)
            {
                throw new AppException(x => x.NotExist(), HttpStatusCode.NotFound);
            }

            return new OkApiResponse(data);
        }


        /// <summary>
        /// Add Sieve filters/sort/pagination and projection to query and get the list
        /// </summary>
        /// <typeparam name="TViewModel"></typeparam>
        /// <param name="sieveModel">sieve model</param>
        /// <param name="select">custom select expression</param>
        /// <returns></returns>
        public async Task<ApiResponse> List<TViewModel>(SieveModel sieveModel, Expression<Func<TDomain, TViewModel>>? select = null)
        {
            var totalRecords = await sieveProcessor
               .Apply(sieveModel, query, null, true, false, false)
               .CountAsync();

            var pageSize = sieveModel.PageSize ?? sieveOptions.DefaultPageSize;
            var totalPages = totalRecords / pageSize + (totalRecords % pageSize == 0 ? 0 : 1);

            var sieveQuery = sieveProcessor.Apply(sieveModel, query);

            var data = await sieveQuery
                .AsNoTracking()
                .Select(select ?? GetSelectExpression<TViewModel>())
                .ToListAsync();

            return new OkApiResponse(data, new { totalRecords, totalPages });
        }

        /// <summary>
        /// create a dynamic expression to handle projection
        /// </summary>
        /// <typeparam name="TViewModel"></typeparam>
        /// <returns></returns>
        private Expression<Func<TDomain, TViewModel>> GetSelectExpression<TViewModel>()
        {
            var paramExpression = Expression.Parameter(typeof(TDomain));

            var viewModelConstructor = typeof(TViewModel).GetConstructor(new[] { typeof(TDomain) });
            if (viewModelConstructor is null)
            {
                throw new ArgumentException("The view model must have a constructor that only accept the domain", nameof(TViewModel));
            }

            var newExpression = Expression.New(viewModelConstructor, paramExpression);

            return Expression.Lambda<Func<TDomain, TViewModel>>(newExpression, paramExpression);
        }
    }
}

﻿using Microsoft.Extensions.Options;
using Sieve.Models;
using Sieve.Services;
using System.Linq;

namespace Web.Services
{
    public class QueryResponseService : IQueryResponseService
    {
        private readonly ISieveProcessor sieveProcessor;
        private readonly IOptions<SieveOptions> sieveOptions;

        public QueryResponseService(
            ISieveProcessor sieveProcessor,
            IOptions<SieveOptions> sieveOptions)
        {
            this.sieveProcessor = sieveProcessor;
            this.sieveOptions = sieveOptions;
        }

        public QueryResponse<TDomain> From<TDomain>(IQueryable<TDomain> query) where TDomain : class
        {
            return new QueryResponse<TDomain>(query, sieveProcessor, sieveOptions);
        }
    }
}

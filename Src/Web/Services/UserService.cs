﻿using Application.Abstraction.Common;
using Domain.Enums;
using Microsoft.AspNetCore.Http;
using System;

namespace Web.Services
{
    class UserService : IUserService
    {
        private readonly HttpContext? context;

        public UserService(IHttpContextAccessor httpContextAccessor)
        {
            context = httpContextAccessor.HttpContext;
            if (context?.User.Identity?.IsAuthenticated ?? false)
            {
                UserId = ReadIdFromClaim(context, "id");
            }
        }
        public int UserId { get; }

        public bool IsInRole(params UserRoleType[] userRoles)
        {
            var result = false;
            foreach (var role in userRoles)
            {
                result = result || (context?.User.IsInRole(Enum.GetName(role)!) ?? false);
            }

            return result;
        }

        private int ReadIdFromClaim(HttpContext context, string name)
        {
            if (int.TryParse(context.User.FindFirst(name)?.Value, out int id))
            {
                return id;
            }
            else
            {
                return 0;
            }
        }
    }
}

using Application;
using Application.Abstraction.Common;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Persistence;
using Sieve.Models;
using Sieve.Services;
using System.Text.Json.Serialization;
using Web.Common;
using Web.Filters;
using Web.Services;
using Web.SieveConfigs;

namespace web
{
    public class Startup
    {
        private readonly IConfiguration configuration;

        public Startup(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddJwtAuthentication(configuration);

            services.Configure<SieveOptions>(configuration.GetSection("Sieve"));
            services.AddScoped<ISieveProcessor, SieveProcessor>();
            services.AddScoped<ISieveProcessor, ApiSieveProcessor>();
            services.AddHttpContextAccessor();
            services.AddScoped<IUserService, UserService>();

            services.AddApplication(configuration);
            services.AddPersistence();
            services.AddInfrastructure();

            services.AddScoped<IQueryResponseService, QueryResponseService>();

            services.AddControllers()
                .AddMvcOptions(opts =>
                {
                    opts.Filters.Add<ValidateModelFilter>();
                })
                .AddJsonOptions(opts =>
                {
                    opts.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                })
                .AddFluentValidation(opts =>
                {
                    opts.AutomaticValidationEnabled = false;
                    opts.RegisterValidatorsFromAssemblyContaining<Startup>();
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/error");
            }

            app.UseRouting();

            var origins = configuration.GetSection("CorsOrigins").Get<string[]>(); ;
            app.UseCors(x => x
                .WithOrigins(origins)
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

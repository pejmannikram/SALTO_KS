﻿using System.Net;

namespace Web.Common
{
    public class OkApiResponse : ApiResponse
    {
        public OkApiResponse(object? data = null, object? metadata = null)
          : base(HttpStatusCode.OK, data, metadata)
        {
        }
    }
}

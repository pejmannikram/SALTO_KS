﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Web.Common
{
    public abstract class ApiResponse : IActionResult
    {
        private readonly HttpStatusCode statusCode;

        public ApiResponse(HttpStatusCode statusCode, object? data = null, object? metadata = null, IEnumerable<ApiError>? errors = null)
        {
            Data = data ?? new object();
            Metadata = metadata;
            Errors = errors;
            this.statusCode = statusCode;
        }

        public object Data { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public object? Metadata { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public IEnumerable<ApiError>? Errors { get; set; }


        public Task ExecuteResultAsync(ActionContext context)
        {
            var options = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
            };

            var response = new JsonResult(this, options)
            {
                StatusCode = (int)statusCode,
            };

            return response.ExecuteResultAsync(context);
        }
    }
}

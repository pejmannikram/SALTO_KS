﻿namespace Web.Common
{
    public class ApiUserRole
    {
        public const string Admin = "Admin";
        public const string User = "User";
    }
}

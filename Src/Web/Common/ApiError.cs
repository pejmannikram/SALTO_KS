﻿using System.Text.Json.Serialization;

namespace Web.Common
{
    public class ApiError
    {
        public ApiError(string message, string? propertyName = null)
        {
            Message = message;
            PropertyName = propertyName;
        }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public string? PropertyName { get; init; }
        public string Message { get; init; }
    }
}

﻿using System.Net;

namespace Web.Common
{
    class CreationApiResponse : ApiResponse
    {
        public CreationApiResponse(int id) : base(HttpStatusCode.Created, new { id })
        {
        }
    }
}

﻿using System.Collections.Generic;
using System.Net;

namespace Web.Common
{
    public class ErrorApiResponse : ApiResponse
    {
        public ErrorApiResponse(HttpStatusCode statusCode, IEnumerable<ApiError> errors)
            : base(statusCode, null, null, errors)
        {
        }
    }
}

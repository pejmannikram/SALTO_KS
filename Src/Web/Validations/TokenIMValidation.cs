﻿using Application.Abstraction.Auth.Models;
using FluentValidation;

namespace Web.Validations
{
    public class TokenIMValidation : AbstractValidator<TokenIM>
    {
        public TokenIMValidation()
        {
            When(model => model.GrantType == GrantType.Password, () =>
            {
                RuleFor(model => model.Username).NotEmpty().EmailAddress();
                RuleFor(model => model.Password).NotEmpty();
            });

            When(model => model.GrantType == GrantType.RefreshToken, () =>
            {
                RuleFor(model => model.RefreshToken).NotEmpty();
            });
        }
    }
}

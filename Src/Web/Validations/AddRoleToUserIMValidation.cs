﻿using Application.Abstraction.UsersRoles.Models;
using FluentValidation;

namespace Web.Validations
{
    public class AddRoleToUserIMValidation : AbstractValidator<AddRoleToUserIM>
    {
        public AddRoleToUserIMValidation()
        {
            RuleFor(x => x.RoleId).NotEmpty().IsInEnum();
        }
    }
}

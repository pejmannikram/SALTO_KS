﻿using Application.Abstraction.Users.Models;
using FluentValidation;

namespace Web.Validations
{
    public class UpdateProfileIMValidation : AbstractValidator<UpdateProfileIM>
    {
        public UpdateProfileIMValidation()
        {
            RuleFor(x => x.FirstName).NotEmpty().Length(3, 30);
            RuleFor(x => x.LastName).NotEmpty().Length(3, 30);
        }
    }
}

﻿using Application.Abstraction.Auth.Models;
using FluentValidation;

namespace Web.Validations
{
    public class RevokeRefreshTokenIMValidation : AbstractValidator<RevokeRefreshTokenIM>
    {
        public RevokeRefreshTokenIMValidation()
        {
            RuleFor(x => x.RefreshToken).NotEmpty();
        }
    }
}

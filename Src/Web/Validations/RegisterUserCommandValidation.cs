﻿using Application.Abstraction.Users.Commands;
using FluentValidation;

namespace Web.Validations
{
    public class RegisterUserCommandValidation : AbstractValidator<RegisterUserCommand>
    {
        public RegisterUserCommandValidation()
        {
            RuleFor(x => x.Email).NotEmpty().EmailAddress();
            RuleFor(x => x.FirstName).NotEmpty().Length(3, 30);
            RuleFor(x => x.LastName).NotEmpty().Length(3, 30);
            RuleFor(x => x.Password).NotEmpty().MinimumLength(8);
            RuleFor(x => x.VerifyCode).NotEmpty().Length(6);
        }
    }
}

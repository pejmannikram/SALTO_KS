﻿using Application.Abstraction.Doors.Commands;
using FluentValidation;

namespace Web.Validations
{
    public class CreateDoorCommandValidation : AbstractValidator<CreateDoorCommand>
    {
        public CreateDoorCommandValidation()
        {
            RuleFor(x => x.ExternalId).NotEmpty();
            RuleFor(x => x.Name).NotEmpty();
        }
    }
}

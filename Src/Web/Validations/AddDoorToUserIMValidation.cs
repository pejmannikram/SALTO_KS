﻿using Application.Abstraction.Doors.Models;
using FluentValidation;

namespace Web.Validations
{
    public class AddDoorToUserIMValidation : AbstractValidator<AddDoorToUserIM>
    {
        public AddDoorToUserIMValidation()
        {
            RuleFor(x => x.DoorId).NotEmpty();
        }
    }
}

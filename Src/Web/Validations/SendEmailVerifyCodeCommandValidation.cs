﻿using Application.Abstraction.Users.Commands;
using FluentValidation;

namespace Web.Validations
{
    public class SendEmailVerifyCodeCommandValidation : AbstractValidator<SendEmailVerifyCodeCommand>
    {
        public SendEmailVerifyCodeCommandValidation()
        {
            RuleFor(x => x.Email).NotEmpty().EmailAddress();
        }
    }
}

﻿using ApiPatchModel;
using FluentValidation;
using FluentValidation.Internal;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Web.Common;

namespace Web.Filters
{
    public class ValidateModelFilter : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var method = context.HttpContext.Request.Method;
            if (method == "PATCH" || method == "POST")
            {
                var result = await GetValidationResult(context);
                if (!result.IsValid)
                {
                    context.Result = ParseErrors(result);
                    return;
                }
            }

            await next();
        }

        private IActionResult ParseErrors(ValidationResult result)
        {
            List<ApiError> errorList = new List<ApiError>();

            foreach (var error in result.Errors)
            {
                errorList.Add(new ApiError(
                    error.ErrorMessage,
                    error.PropertyName)
                    );
            }

            return new ErrorApiResponse(HttpStatusCode.BadRequest, errorList);
        }

        private object? GetInputModel(IDictionary<string, object> actionArguments)
        {
            foreach (var (_, value) in actionArguments)
            {
                if (!value.GetType().IsValueType)
                {
                    return value;
                }
            }

            return null;
        }

        private async Task<ValidationResult> GetValidationResult(ActionExecutingContext context)
        {
            var inputModel = GetInputModel(context.ActionArguments);
            if (inputModel is IPatchModel patchModel)
            {
                var result = await RunValidator(context.HttpContext.RequestServices, patchModel.GetModel(), patchModel.ModelType);

                // we need just return validation error for
                // properties that actually touched by PATH request
                if (!result.IsValid)
                {
                    var filteredErrors = result
                        .Errors
                        .Where(x => patchModel.HasProperty(x.PropertyName)
                        );
                    result = new ValidationResult(filteredErrors);
                }

                return result;
            }
            else if (inputModel is not null)
            {
                return await RunValidator(context.HttpContext.RequestServices, inputModel, inputModel.GetType());
            }

            return new ValidationResult();
        }
        private async Task<ValidationResult> RunValidator(IServiceProvider serviceProvider, object model, Type modelType)
        {
            var validatorFactory = serviceProvider.GetRequiredService<IValidatorFactory>();
            var validator = validatorFactory.GetValidator(modelType);

            if (validator is null)
            {
                return new ValidationResult();
            }
            var selector = ValidatorOptions.Global.ValidatorSelectors.DefaultValidatorSelectorFactory();
            var context = new ValidationContext<object>(model, new PropertyChain(), selector);
            context.SetServiceProvider(serviceProvider);

            return await validator.ValidateAsync(context);
        }
    }
}

﻿using Application.Abstraction.Auth.Models;
using Application.Abstraction.Common;
using Application.Common;
using Domain.Entities;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace Application.Auth.Services
{
    public class RefreshTokenService : IService, IRefreshTokenService
    {
        private readonly IPasswordHasher<RefreshToken> passwordHasher;
        private readonly IDateTime dateTime;
        private readonly IAPIDbContext context;

        public RefreshTokenService(
            IPasswordHasher<RefreshToken> passwordHasher,
            IDateTime dateTime,
            IAPIDbContext context
            )
        {
            this.passwordHasher = passwordHasher;
            this.dateTime = dateTime;
            this.context = context;
        }

        public async Task<AuthTokenVM> PersistRefreshToken(int userId, AuthTokenVM token)
        {
            var refreshToken = new RefreshToken(userId);
            var hashedRefreshToken = passwordHasher.HashPassword(refreshToken, token.RefreshToken);
            refreshToken.UpdateToken(hashedRefreshToken, dateTime.Now);

            await context.RefreshTokens.AddAsync(refreshToken);

            // we need refresh token id
            // refresh token is = id + random string
            // this id uses to identify refresh token 
            // without the need to validate them one by one 
            // by password checker
            await context.SaveChangesAsync();
            token.SetRefreshTokenId(refreshToken.Id);

            return token;
        }

    }
}

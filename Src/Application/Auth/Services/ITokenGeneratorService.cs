﻿using Application.Abstraction.Auth.Models;
using Domain.Enums;
using System.Collections.Generic;

namespace Application.Auth.Services
{
    public interface ITokenGeneratorService
    {
        AuthTokenVM GenerateAuthToken(int userId, IEnumerable<UserRoleType> roles);
    }
}

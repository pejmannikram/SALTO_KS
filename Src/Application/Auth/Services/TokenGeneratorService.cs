﻿using Application.Abstraction.Auth.Models;
using Application.Abstraction.Common;
using Application.Common;
using Domain.Enums;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;

namespace Application.Auth.Services
{
    class TokenGeneratorService : IService, ITokenGeneratorService
    {
        private readonly AppOption options;
        private readonly IDateTime dateTime;

        public TokenGeneratorService(IOptions<AppOption> options, IDateTime dateTime)
        {
            this.options = options.Value;
            this.dateTime = dateTime;
        }

        private Claim[] GetClaims(int userId, IEnumerable<UserRoleType> roles)
        {
            return roles
                 .Select(role => new Claim(ClaimTypes.Role, Enum.GetName(role) ?? ""))
                 .Append(new Claim("id", userId.ToString()))
                 .ToArray();
        }

        public AuthTokenVM GenerateAuthToken(int userId, IEnumerable<UserRoleType> roles)
        {
            var expiration = dateTime.Now.AddMinutes(45);

            var accessToken = GenerateJwtToken(userId, roles, expiration);
            var refreshToken = GenerateRefreshToken();

            return new AuthTokenVM(accessToken, refreshToken, expiration);
        }

        private string GenerateJwtToken(int userId, IEnumerable<UserRoleType> roles, System.DateTime expiration)
        {
            var rsa = new RSACryptoServiceProvider();

            rsa.ImportPkcs8PrivateKey(Convert.FromBase64String(options.PrivateKey), out _);
            var signingKey = new RsaSecurityKey(rsa);
            var signingCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.RsaSha256Signature, SecurityAlgorithms.Sha256Digest);

            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(GetClaims(userId, roles)),
                Expires = expiration,
                SigningCredentials = signingCredentials,
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            var result = tokenHandler.WriteToken(token);

            return result;
        }

        private string GenerateRefreshToken()
        {
            using var rng = new RNGCryptoServiceProvider();
            var bytes = new byte[64];
            rng.GetBytes(bytes);

            return Convert.ToBase64String(bytes);
        }

    }
}

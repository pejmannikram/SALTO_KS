﻿using Application.Abstraction.Auth.Models;
using System.Threading.Tasks;

namespace Application.Auth.Services
{
    public interface IRefreshTokenService
    {
        Task<AuthTokenVM> PersistRefreshToken(int userId, AuthTokenVM token);
    }
}
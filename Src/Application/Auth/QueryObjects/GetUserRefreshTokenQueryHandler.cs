﻿using Application.Abstraction.Auth.QueryObjects;
using Application.Abstraction.Common;
using CommandR;
using Domain.Entities;
using EFQueryObject;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Auth.QueryObjects
{
    class GetUserRefreshTokenQueryHandler : IQueryObjectHandler<GetUserRefreshTokenQuery, RefreshToken>
    {
        private readonly IAPIDbContext context;

        public GetUserRefreshTokenQueryHandler(IAPIDbContext context)
        {
            this.context = context;
        }

        ValueTask<IQueryable<RefreshToken>> ICommandHandler<GetUserRefreshTokenQuery, IQueryable<RefreshToken>>.Handle(GetUserRefreshTokenQuery request)
        {
            var query = context.RefreshTokens.Where(x => x.Id == request.RefreshTokenId);
            return ValueTask.FromResult(query);
        }
    }
}

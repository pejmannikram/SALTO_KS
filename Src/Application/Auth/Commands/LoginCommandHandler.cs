﻿using Application.Abstraction.Auth.Commands;
using Application.Abstraction.Auth.Models;
using Application.Abstraction.Common;
using Application.Abstraction.Users.QueryObjects;
using Application.Auth.Services;
using CommandR;
using Domain.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Application.Auth.Commands
{
    public class LoginCommandHandler : ICommandHandler<LoginCommand, AuthTokenVM>
    {
        private readonly IMediator mediator;
        private readonly IRefreshTokenService refreshToken;
        private readonly IPasswordHasher<User> passwordHasher;
        private readonly ITokenGeneratorService tokenGeneratorService;

        public LoginCommandHandler(
            IMediator mediator,
            IRefreshTokenService refreshToken,
            IPasswordHasher<User> passwordHasher,
            ITokenGeneratorService tokenGeneratorService)
        {
            this.mediator = mediator;
            this.refreshToken = refreshToken;
            this.passwordHasher = passwordHasher;
            this.tokenGeneratorService = tokenGeneratorService;
        }

        public async ValueTask<AuthTokenVM> Handle(LoginCommand request)
        {
            var query = await mediator.Send(new GetUserQuery());
            var user = await query
                .Where(x => x.Email == request.Email)
                .Include(x => x.UserRoles)
                .FirstOrDefaultAsync();

            if (user is not null && passwordHasher.VerifyHashedPassword(user, user.Password, request.Password)
                == PasswordVerificationResult.Success)
            {
                var token = tokenGeneratorService.GenerateAuthToken(user.Id, user.UserRoles.Select(x => x.RoleId));
                token = await refreshToken.PersistRefreshToken(user.Id, token);

                return token;
            }

            throw new AppException(x => x.WrongCredential(), HttpStatusCode.Unauthorized);
        }
    }
}

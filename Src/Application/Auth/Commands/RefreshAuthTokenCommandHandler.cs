﻿using Application.Abstraction.Auth.Commands;
using Application.Abstraction.Auth.Models;
using Application.Abstraction.Auth.QueryObjects;
using Application.Abstraction.Common;
using Application.Abstraction.UsersRoles.QueryObjects;
using Application.Auth.Services;
using Application.Common;
using CommandR;
using Domain.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Net;
using System.Threading.Tasks;


namespace Application.Auth.Commands
{
    public class RefreshAuthTokenCommandHandler : ICommandHandler<RefreshAuthTokenCommand, AuthTokenVM>
    {
        private readonly IMediator mediator;
        private readonly IDateTime dateTime;
        private readonly ITokenGeneratorService tokenGenerator;
        private readonly IPasswordHasher<RefreshToken> passwordHasher;

        public RefreshAuthTokenCommandHandler(
            IMediator mediator,
            IDateTime dateTime,
            ITokenGeneratorService tokenGenerator,
            IPasswordHasher<RefreshToken> passwordHasher
            )
        {
            this.mediator = mediator;
            this.dateTime = dateTime;
            this.tokenGenerator = tokenGenerator;
            this.passwordHasher = passwordHasher;
        }

        public async ValueTask<AuthTokenVM> Handle(RefreshAuthTokenCommand request)
        {
            var (token, id) = request.RefreshToken;
            var query = await mediator.Send(new GetUserRefreshTokenQuery(id));
            var refreshToken = await query.FirstOrDefaultAsync();

            if (refreshToken is not null && refreshToken.ExpirationDate > dateTime.Now &&
                passwordHasher.VerifyHashedPassword(refreshToken, refreshToken.Token, token)
                == PasswordVerificationResult.Success)
            {
                var rolesQuery = await mediator.Send(new GetUsersRolesQuery(refreshToken.UserId));
                var userRoles = await rolesQuery.ToArrayAsync();

                var authToken = tokenGenerator.GenerateAuthToken(refreshToken.UserId, userRoles.Select(x => x.RoleId));
                authToken.SetRefreshTokenId(refreshToken.Id);

                var hashedRefreshToken = passwordHasher.HashPassword(refreshToken, authToken.RefreshToken);
                refreshToken.UpdateToken(hashedRefreshToken, dateTime.Now);

                return authToken;
            }

            throw new AppException(x => x.WrongRefreshToken(), HttpStatusCode.Unauthorized);
        }
    }
}

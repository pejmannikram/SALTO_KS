﻿using Application.Abstraction.Auth.Commands;
using Application.Abstraction.Auth.QueryObjects;
using Application.Abstraction.Common;
using CommandR;
using Domain.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Auth.Commands
{
    public class RevokeRefreshTokenCommandHandler : ICommandHandler<RevokeRefreshTokenCommand, VoidResult>
    {
        private readonly IMediator mediator;
        private readonly IAPIDbContext context;
        private readonly IPasswordHasher<RefreshToken> passwordHasher;
        private readonly IUserService user;

        public RevokeRefreshTokenCommandHandler(
            IMediator mediator,
            IAPIDbContext context,
            IPasswordHasher<RefreshToken> passwordHasher,
            IUserService user
            )
        {
            this.mediator = mediator;
            this.context = context;
            this.passwordHasher = passwordHasher;
            this.user = user;
        }


        public async ValueTask<VoidResult> Handle(RevokeRefreshTokenCommand request)
        {
            var (token, id) = request.RefreshToken;
            var query = await mediator.Send(new GetUserRefreshTokenQuery(id));
            var refreshToken = await query
                .Where(x => x.UserId == user.UserId)
                .FirstOrDefaultAsync();

            if (refreshToken is not null &&
                passwordHasher.VerifyHashedPassword(refreshToken, refreshToken.Token, token)
                == PasswordVerificationResult.Success)
            {
                context.RefreshTokens.Remove(refreshToken);
            }


            return VoidResult.Value;
        }
    }
}

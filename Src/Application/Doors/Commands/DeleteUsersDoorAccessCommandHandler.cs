﻿using Application.Abstraction.Common;
using Application.Abstraction.Doors.Commands;
using Application.Abstraction.Doors.QueryObjects;
using Application.Common;
using CommandR;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Doors.Commands
{
    public class DeleteUsersDoorAccessCommandHandler : ICommandHandler<DeleteUsersDoorAccessCommand, UserDoor>
    {
        private readonly IAPIDbContext context;
        private readonly IMediator mediator;

        public DeleteUsersDoorAccessCommandHandler(IAPIDbContext context, IMediator mediator)
        {
            this.context = context;
            this.mediator = mediator;
        }

        public async ValueTask<UserDoor> Handle(DeleteUsersDoorAccessCommand request)
        {
            var query = await mediator.Send(new GetUsersDoorsQuery(request.UserId));
            var userDoor = await query
            .Where(x => x.DoorId == request.DoorId)
            .FirstOrDefaultAsync();

            Guard.Argument(nameof(UserDoor)).NotNull(userDoor);

            context.UserDoors.Remove(userDoor);

            return userDoor;
        }
    }
}

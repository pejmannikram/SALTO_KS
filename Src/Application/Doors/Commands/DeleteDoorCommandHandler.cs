﻿using Application.Abstraction.Common;
using Application.Abstraction.Doors.Commands;
using Application.Abstraction.Doors.QueryObjects;
using Application.Common;
using CommandR;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Doors.Commands
{
    public class DeleteDoorCommandHandler : ICommandHandler<DeleteDoorCommand, Door>
    {
        private readonly IAPIDbContext context;
        private readonly IMediator mediator;

        public DeleteDoorCommandHandler(
            IAPIDbContext context,
            IMediator mediator)
        {
            this.context = context;
            this.mediator = mediator;
        }

        public async ValueTask<Door> Handle(DeleteDoorCommand request)
        {
            var query = await mediator.Send(new GetDoorQuery());
            var door = await query
            .Where(x => x.Id == request.Id)
            .FirstOrDefaultAsync();

            Guard.Argument(nameof(Door)).NotNull(door);

            // for better performance, we need to delete bulk records with raw SQL command            
            await context.ExecuteSqlRaw($@"DELETE FROM ""{nameof(context.UserDoors)}"" WHERE ""{nameof(UserDoor.DoorId)}"" = @0", request.Id);
            await context.ExecuteSqlRaw($@"DELETE FROM ""{nameof(context.DoorAccessHistories)}"" WHERE ""{nameof(DoorAccessHistory.DoorId)}"" = @0", request.Id);

            context.Doors.Remove(door);

            return door;
        }
    }
}

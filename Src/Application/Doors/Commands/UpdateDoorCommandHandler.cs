using Application.Abstraction.Doors.Commands;
using Application.Abstraction.Doors.QueryObjects;
using Application.Common;
using CommandR;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Doors.Commands
{
    public class UpdateDoorCommandHandler : ICommandHandler<UpdateDoorCommand, Door>
    {
        private readonly IMediator mediator;

        public UpdateDoorCommandHandler(IMediator mediator)
        {
            this.mediator = mediator;
        }

        public async ValueTask<Door> Handle(UpdateDoorCommand request)
        {
            var query = await mediator.Send(new GetDoorQuery());
            var door = await query
                .Where(x => x.Id == request.Id)
                .FirstOrDefaultAsync();

            Guard.Argument(nameof(Door)).NotNull(door);

            if (request.PatchModel.IsChanged(x => x.Name, door.Name) &&
                request.PatchModel.TryGetValue(x => x.Name, out var name))
            {
                await Guard.Argument(nameof(Door.Name))
                 .NotExist(query
                     .Where(x => x.Name.ToLower() == name.ToLower())
                     .Where(x => x.Id != door.Id)
                     );
            }

            request.PatchModel.MapTo(door);

            return door;
        }
    }
}

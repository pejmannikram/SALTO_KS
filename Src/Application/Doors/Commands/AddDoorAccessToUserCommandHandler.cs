﻿using Application.Abstraction.Common;
using Application.Abstraction.Doors.Commands;
using Application.Abstraction.Doors.QueryObjects;
using Application.Abstraction.Users.QueryObjects;
using Application.Common;
using CommandR;
using Domain.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Doors.Commands
{
    public class AddDoorAccessToUserCommandHandler : ICommandHandler<AddDoorAccessToUserCommand, UserDoor>
    {
        private readonly IMediator mediator;
        private readonly IAPIDbContext context;

        public AddDoorAccessToUserCommandHandler(
            IMediator mediator,
            IAPIDbContext context)
        {
            this.mediator = mediator;
            this.context = context;
        }

        public async ValueTask<UserDoor> Handle(AddDoorAccessToUserCommand request)
        {
            var doorQuery = await mediator.Send(new GetDoorQuery());
            await Guard.Argument(nameof(Door)).Exist(doorQuery.Where(x => x.Id == request.DoorId));

            var userQuery = await mediator.Send(new GetUserQuery());
            await Guard.Argument(nameof(User)).Exist(userQuery.Where(x => x.Id == request.UserId));

            var query = await mediator.Send(new GetUsersDoorsQuery(request.UserId));
            await Guard.Argument(nameof(UserDoor)).NotExist(query.Where(x => x.DoorId == request.DoorId));

            var result = new UserDoor(request.UserId, request.DoorId);

            await context.UserDoors.AddAsync(result);

            return result;
        }
    }
}

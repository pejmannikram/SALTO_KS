using Application.Abstraction.Common;
using Application.Abstraction.Doors.Commands;
using Application.Abstraction.Doors.QueryObjects;
using Application.Common;
using CommandR;
using Domain.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Doors.Commands
{
    public class CreateDoorCommandHandler : ICommandHandler<CreateDoorCommand, Door>
    {
        private readonly IMediator mediator;
        private readonly IAPIDbContext context;

        public CreateDoorCommandHandler(
            IMediator mediator,
            IAPIDbContext context)
        {
            this.mediator = mediator;
            this.context = context;
        }

        public async ValueTask<Door> Handle(CreateDoorCommand request)
        {
            var query = await mediator.Send(new GetDoorQuery());
            await Guard.Argument(nameof(request.Name))
                .NotExist(query.Where(x => x.Name.ToLower() == request.Name.ToLower()));

            var result = new Door(request.Name, request.ExternalId);

            await context.Doors.AddAsync(result);

            return result;
        }
    }
}

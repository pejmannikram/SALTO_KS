﻿using Application.Abstraction.Common;
using Application.Abstraction.DoorClient;
using Application.Abstraction.Doors.Commands;
using Application.Abstraction.Doors.QueryObjects;
using Application.Common;
using CommandR;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Application.Doors.Commands
{
    public class OpenDoorCommandHandler : ICommandHandler<OpenDoorCommand, VoidResult>
    {
        private readonly IAPIDbContext context;
        private readonly IMediator mediator;
        private readonly IDoorClientService doorClient;
        private readonly IUserService user;

        public OpenDoorCommandHandler(
            IAPIDbContext context,
            IMediator mediator,
            IDoorClientService doorClient,
            IUserService user)
        {
            this.context = context;
            this.mediator = mediator;
            this.doorClient = doorClient;
            this.user = user;
        }

        public async ValueTask<VoidResult> Handle(OpenDoorCommand request)
        {
            var query = await mediator.Send(new GetDoorByUserQuery(user.UserId, request.Id));
            var door = await query.FirstOrDefaultAsync();

            Guard.Argument(nameof(Door)).NotNull(door);

            await doorClient.Open(door.ExternalId);
            await context.DoorAccessHistories.AddAsync(new DoorAccessHistory(user.UserId, request.Id));

            return VoidResult.Value;
        }
    }
}

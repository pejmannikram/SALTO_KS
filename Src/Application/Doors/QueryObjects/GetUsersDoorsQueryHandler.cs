using Application.Abstraction.Common;
using Application.Abstraction.Doors.QueryObjects;
using Domain.Entities;
using EFQueryObject;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Doors.QueryObjects
{
    public class GetUsersDoorsQueryHandler : IQueryObjectHandler<GetUsersDoorsQuery, UserDoor>
    {
        private readonly IAPIDbContext context;

        public GetUsersDoorsQueryHandler(IAPIDbContext context)
        {
            this.context = context;
        }

        public ValueTask<IQueryable<UserDoor>> Handle(GetUsersDoorsQuery request)
        {
            var query = context.UserDoors
                .Where(x => x.UserId == request.UserId);

            return ValueTask.FromResult(query);
        }
    }
}

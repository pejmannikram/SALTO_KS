﻿using Application.Abstraction.Common;
using Application.Abstraction.Doors.QueryObjects;
using Domain.Entities;
using EFQueryObject;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Doors.QueryObjects
{
    public class GetDoorByUserQueryHandler : IQueryObjectHandler<GetDoorByUserQuery, Door>
    {
        private readonly IAPIDbContext context;

        public GetDoorByUserQueryHandler(IAPIDbContext context)
        {
            this.context = context;
        }

        public ValueTask<IQueryable<Door>> Handle(GetDoorByUserQuery request)
        {
            var query = context.UserDoors
                .Where(x => x.DoorId == request.DoorId)
                .Where(x => x.UserId == request.UserId)
                .Select(x => x.Door);

            return ValueTask.FromResult(query);
        }
    }
}

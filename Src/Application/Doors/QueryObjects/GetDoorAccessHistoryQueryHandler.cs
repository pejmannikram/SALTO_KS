﻿using Application.Abstraction.Common;
using Application.Abstraction.Doors.QueryObjects;
using Domain.Entities;
using EFQueryObject;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Doors.QueryObjects
{
    public class GetDoorAccessHistoryQueryHandler : IQueryObjectHandler<GetDoorAccessHistoryQuery, DoorAccessHistory>
    {
        private readonly IAPIDbContext context;

        public GetDoorAccessHistoryQueryHandler(IAPIDbContext context)
        {
            this.context = context;
        }

        public ValueTask<IQueryable<DoorAccessHistory>> Handle(GetDoorAccessHistoryQuery request)
        {
            var query = context.DoorAccessHistories
                .Where(x => x.DoorId == request.DoorId);

            return ValueTask.FromResult(query);
        }
    }
}

using Application.Abstraction.Common;
using Application.Abstraction.Doors.QueryObjects;
using Application.Abstraction.UsersRoles.Models;
using CommandR;
using Domain.Entities;
using Domain.Enums;
using EFQueryObject;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Doors.QueryObjects
{
    public class GetDoorQueryHandler : IQueryObjectHandler<GetDoorQuery, Door>
    {
        private readonly IAPIDbContext context;
        private readonly IUserService userService;
        private readonly IMediator mediator;

        public GetDoorQueryHandler(
            IAPIDbContext context,
            IUserService userService,
            IMediator mediator
            )
        {
            this.context = context;
            this.userService = userService;
            this.mediator = mediator;
        }

        public async ValueTask<IQueryable<Door>> Handle(GetDoorQuery request)
        {
            var query = context.Doors.AsQueryable();
             
            if (!userService.IsInRole(UserRoleType.Admin))
            {
                var userDoorQuery = await mediator.Send(new GetUsersDoorsQuery(userService.UserId));
                query = query.Where(x => userDoorQuery.Any(us => us.DoorId == x.Id));
            }

            return query;
        }
    }
}

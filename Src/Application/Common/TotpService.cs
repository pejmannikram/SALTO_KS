﻿using Application.Abstraction.Common;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Options;
using OtpNet;
using System;
using System.Text;
using System.Threading.Tasks;

namespace Application.Common
{
    class TotpService : IService, ITotpService
    {
        private readonly IDistributedCache cache;
        private readonly IDateTime dateTime;
        private readonly AppOption options;

        public TotpService(IDistributedCache cache, IDateTime dateTime, IOptions<AppOption> options)
        {
            this.cache = cache;
            this.dateTime = dateTime;
            this.options = options.Value;
        }


        public async Task<string> Compute(string key = "")
        {
            var otp = CreateTotpInstance(key);
            var token = otp.ComputeTotp(dateTime.Now);

            var options = new DistributedCacheEntryOptions { AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(4) };
            await cache.SetStringAsync(CreateCacheKey(token), key, options);

            return token;
        }


        public async Task<bool> Verify(string totp, string key = "")
        {
            var cacheKey = CreateCacheKey(totp);
            var cached = await cache.GetStringAsync(cacheKey);

            // already used
            if (cached != key)
            {
                return false;
            }

            var otp = CreateTotpInstance(key);
            var verified = otp.VerifyTotp(dateTime.Now, totp, out var _, new VerificationWindow(2, 2));

            if (verified)
            {
                await cache.RemoveAsync(cacheKey);
                return true;
            }

            return false;
        }

        private string CreateCacheKey(string key)
        {
            return "totp:" + key;
        }

        private Totp CreateTotpInstance(string key)
        {
            var bytes = Encoding.ASCII.GetBytes(options.PrivateKey.Substring(0, 10) + "_" + key);
            return new Totp(bytes, mode: OtpHashMode.Sha512);
        }
    }
}

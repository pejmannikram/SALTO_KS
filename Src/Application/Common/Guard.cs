﻿using Application.Abstraction.Common;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Application.Common
{
    public class Guard
    {
        private readonly string name;

        private Guard(string name)
        {
            this.name = name;
        }

        public static Guard Argument(string name)
        {
            return new Guard(name);
        }

        public void NotNull<T>([NotNull] T? value)
        {
            if (value is null)
            {
                throw new AppException(x => x.NotExist(name), HttpStatusCode.NotFound);
            }
        }

        public async Task NotExist<T>(IQueryable<T> query)
        {
            if (await query.AnyAsync())
            {
                throw new AppException(x => x.IsDuplicate(name), HttpStatusCode.Conflict);
            }
        }

        public async Task Exist<T>(IQueryable<T> query)
        {
            if (!await query.AnyAsync())
            {
                throw new AppException(x => x.NotExist(name), HttpStatusCode.NotFound);
            }
        }
    }
}

﻿using Application.Abstraction.Common;
using CommandR;
using EFQueryObject;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Common
{
    public class SaveDBCommandPipeline<TRequest, TResponse> : ICommandPipeline<TRequest, TResponse>
    {
        private readonly IAPIDbContext context;

        public SaveDBCommandPipeline(IAPIDbContext context)
        {
            this.context = context;
        }

        public async ValueTask<TResponse> Handle(TRequest request, PipelineDelegate<TResponse> next)
        {
            var isQuery = typeof(TRequest).GetInterfaces().Any(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(IQueryObject<>));
            if (!isQuery)
            {
                var transaction = await context.BeginTransaction();
                var result = await next();

                await context.SaveChangesAsync();
                await transaction.CommitAsync();

                return result;
            }

            return await next();
        }
    }
}

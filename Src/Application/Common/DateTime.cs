﻿using Application.Abstraction.Common;

namespace Application.Common
{
    public class DateTime : IService, IDateTime
    {
        public System.DateTime Now => System.DateTime.UtcNow;
    }
}

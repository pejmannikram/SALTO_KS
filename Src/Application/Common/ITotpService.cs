﻿using System.Threading.Tasks;

namespace Application.Common
{
    interface ITotpService
    {
        /// <summary>
        /// Compute TOTP code with default key
        /// </summary>
        /// <param name="key">it will be added to the default key</param>
        /// <returns></returns>
        Task<string> Compute(string key = "");

        /// <summary>
        /// Verify TOTP code
        /// </summary>
        /// <param name="totp">the token</param>
        /// <param name="key">it will be added to the default key</param>      
        /// <returns></returns>
        Task<bool> Verify(string totp, string key = "");
    }
}

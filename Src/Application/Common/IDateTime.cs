﻿namespace Application.Common
{
    public interface IDateTime
    {
        System.DateTime Now { get; }
    }
}

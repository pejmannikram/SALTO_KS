﻿using Application.Abstraction.Common;
using Application.Abstraction.Email;
using Application.Email;
using System.Collections.Generic;

namespace Application.Users.Common
{
    class UserEmailContentProvider : IUserEmailContentProvider, IService
    {
        private readonly IEmailService emailService;

        public UserEmailContentProvider(IEmailService emailService)
        {
            this.emailService = emailService;
        }

        public EmailProviderSender Register()
        {
            return new EmailProviderSender(emailService, new EmailContent("Welcome to ...", "..."));
        }

        public EmailProviderSender VerifyEmail(string token)
        {
            return new EmailProviderSender(emailService, new EmailContent("Verify Your Email Address", "...", new Dictionary<string, string> { { "token", token } }));
        }
    }
}

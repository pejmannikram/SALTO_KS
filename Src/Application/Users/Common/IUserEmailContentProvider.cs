﻿using Application.Email;

namespace Application.Users.Common
{
    interface IUserEmailContentProvider
    {
        EmailProviderSender Register();
        EmailProviderSender VerifyEmail(string token);
    }
}

﻿using Application.Abstraction.Auth.Models;
using Application.Abstraction.Common;
using Application.Abstraction.Users.Commands;
using Application.Abstraction.Users.QueryObjects;
using Application.Auth.Services;
using Application.Common;
using Application.Users.Common;
using CommandR;
using Domain.Entities;
using Domain.Enums;
using Microsoft.AspNetCore.Identity;
using System.Net;
using System.Threading.Tasks;

namespace Application.Users.Commands
{
    class RegisterUserCommandHandler : ICommandHandler<RegisterUserCommand, AuthTokenVM>
    {
        private readonly IAPIDbContext context;
        private readonly ITotpService totp;
        private readonly IMediator mediator;
        private readonly IRefreshTokenService refreshToken;
        private readonly IPasswordHasher<User> passwordHasher;
        private readonly ITokenGeneratorService tokenGenerator;
        private readonly IUserEmailContentProvider userEmailContent;

        public RegisterUserCommandHandler(
            IAPIDbContext context,
            ITotpService totp,
            IMediator mediator,
            IRefreshTokenService refreshToken,
            IPasswordHasher<User> passwordHasher,
            ITokenGeneratorService tokenGenerator,
            IUserEmailContentProvider userEmailContent)
        {
            this.context = context;
            this.totp = totp;
            this.mediator = mediator;
            this.refreshToken = refreshToken;
            this.passwordHasher = passwordHasher;
            this.tokenGenerator = tokenGenerator;
            this.userEmailContent = userEmailContent;
        }

        public async ValueTask<AuthTokenVM> Handle(RegisterUserCommand request)
        {
            var isTokenValid = await totp.Verify(request.VerifyCode, request.Email);
            if (!isTokenValid)
            {
                throw new AppException(x => x.TokenIsNotValid(), HttpStatusCode.BadRequest);
            }

            var query = await mediator.Send(new GetUserByEmailQuery(request.Email));
            await Guard.Argument(nameof(request.Email)).NotExist(query);

            var user = new User(request.FirstName, request.LastName, request.Email);
            user.SetPassword(passwordHasher.HashPassword(user, request.Password));

            await context.Users.AddAsync(user);
            await context.UserRoles.AddAsync(new UserRole(UserRoleType.User, user));

            // need user id to generate auth token
            await context.SaveChangesAsync();

            var token = tokenGenerator.GenerateAuthToken(user.Id, new[] { UserRoleType.User });
            token = await refreshToken.PersistRefreshToken(user.Id, token);

            await userEmailContent.Register().Send(request.Email);

            return token;
        }
    }
}

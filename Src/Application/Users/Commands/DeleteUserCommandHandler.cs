﻿using Application.Abstraction.Common;
using Application.Abstraction.Users.Commands;
using Application.Abstraction.Users.QueryObjects;
using Application.Common;
using CommandR;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Application.Users.Commands
{
    class DeleteUserCommandHandler : ICommandHandler<DeleteUserCommand, User>
    {
        private readonly IAPIDbContext context;
        private readonly IUserService userService;
        private readonly IMediator mediator;

        public DeleteUserCommandHandler(
            IAPIDbContext context,
            IUserService userService,
            IMediator mediator)
        {
            this.context = context;
            this.userService = userService;
            this.mediator = mediator;
        }

        public async ValueTask<User> Handle(DeleteUserCommand request)
        {
            if (userService.UserId == request.Id)
            {
                throw new AppException(x => x.CanNotRemoveYourSelf(), HttpStatusCode.Forbidden);
            }

            var query = await mediator.Send(new GetUserQuery());
            var user = await query
                .Where(x => x.Id == request.Id)
                .FirstOrDefaultAsync();

            Guard.Argument(nameof(User)).NotNull(user);

            // for better performance, we need to delete bulk records with raw SQL command
            await context.ExecuteSqlRaw($@"DELETE FROM ""{nameof(context.UserRoles)}"" WHERE ""{nameof(UserRole.UserId)}"" = @0", request.Id);
            await context.ExecuteSqlRaw($@"DELETE FROM ""{nameof(context.UserDoors)}"" WHERE ""{nameof(UserDoor.UserId)}"" = @0", request.Id);
            await context.ExecuteSqlRaw($@"DELETE FROM ""{nameof(context.DoorAccessHistories)}"" WHERE ""{nameof(DoorAccessHistory.UserId)}"" = @0", request.Id);
            await context.ExecuteSqlRaw($@"DELETE FROM ""{nameof(context.RefreshTokens)}"" WHERE ""{nameof(RefreshToken.UserId)}"" = @0", request.Id);

            context.Users.Remove(user);

            return user;
        }
    }
}

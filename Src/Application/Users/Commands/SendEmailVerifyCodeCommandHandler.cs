﻿using Application.Abstraction.Users.Commands;
using Application.Abstraction.Users.QueryObjects;
using Application.Common;
using Application.Users.Common;
using CommandR;
using System.Threading.Tasks;

namespace Application.Users.Commands
{
    class SendEmailVerifyCodeCommandHandler : ICommandHandler<SendEmailVerifyCodeCommand, VoidResult>
    {
        private readonly ITotpService totp;
        private readonly IMediator mediator;
        private readonly IUserEmailContentProvider userEmailContent;

        public SendEmailVerifyCodeCommandHandler(
            ITotpService totp,
            IMediator mediator,
            IUserEmailContentProvider userEmailContent)
        {
            this.totp = totp;
            this.mediator = mediator;
            this.userEmailContent = userEmailContent;
        }

        public async ValueTask<VoidResult> Handle(SendEmailVerifyCodeCommand request)
        {
            var query = await mediator.Send(new GetUserByEmailQuery(request.Email));
            await Guard.Argument(nameof(request.Email)).NotExist(query);

            //Create a time - one time password and email it based on user email
            var token = await totp.Compute(request.Email);

            await userEmailContent
                .VerifyEmail(token)
                .Send(request.Email);

            return VoidResult.Value;
        }
    }
}

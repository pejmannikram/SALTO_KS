﻿using Application.Abstraction.Common;
using Application.Abstraction.Users.Commands;
using Application.Abstraction.Users.QueryObjects;
using Application.Common;
using CommandR;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Users.Commands
{
    class UpdateProfileCommandHandler : ICommandHandler<UpdateProfileCommand, User>
    {
        private readonly IUserService userService;
        private readonly IMediator mediator;

        public UpdateProfileCommandHandler(IUserService userService, IMediator mediator)
        {
            this.userService = userService;
            this.mediator = mediator;
        }

        public async ValueTask<User> Handle(UpdateProfileCommand request)
        {
            var query = await mediator.Send(new GetUserQuery());
            var user = await query
                .Where(x => x.Id == userService.UserId)
                .FirstOrDefaultAsync();

            Guard.Argument(nameof(User)).NotNull(user);

            request.PatchModel.MapTo(user);

            return user;
        }
    }
}

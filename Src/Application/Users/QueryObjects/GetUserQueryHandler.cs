﻿using Application.Abstraction.Common;
using Application.Abstraction.Users.QueryObjects;
using Domain.Entities;
using EFQueryObject;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Users.QueryObjects
{
    public class GetUserQueryHandler : IQueryObjectHandler<GetUserQuery, User>
    {
        private readonly IAPIDbContext context;

        public GetUserQueryHandler(IAPIDbContext context)
        {
            this.context = context;
        }

        public ValueTask<IQueryable<User>> Handle(GetUserQuery request)
        {
            var query = context.Users.AsQueryable();

            return ValueTask.FromResult(query);
        }
    }
}

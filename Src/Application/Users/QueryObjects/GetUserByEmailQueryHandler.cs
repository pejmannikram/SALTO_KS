﻿using Application.Abstraction.Common;
using Application.Abstraction.Users.QueryObjects;
using Domain.Entities;
using EFQueryObject;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Users.QueryObjects
{
    public class GetUserByEmailQueryHandler : IQueryObjectHandler<GetUserByEmailQuery, User>
    {
        private readonly IAPIDbContext context;

        public GetUserByEmailQueryHandler(IAPIDbContext context)
        {
            this.context = context;
        }

        public ValueTask<IQueryable<User>> Handle(GetUserByEmailQuery request)
        {
            var query = context.Users.Where(x => x.Email.ToLower() == request.Email.ToLower());

            return ValueTask.FromResult(query);
        }
    }
}

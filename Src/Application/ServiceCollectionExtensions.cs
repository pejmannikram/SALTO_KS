﻿
using Application.Abstraction.Common;
using EFQueryObject;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Application
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddApplication(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddOptions<AppOption>().Configure(x => configuration.Bind(x));
            services.AddScoped(typeof(IPasswordHasher<>), typeof(PasswordHasher<>));

            services.Scan(scan => scan
             .FromCallingAssembly()
             .AddClasses(classes => classes.AssignableTo<IService>())
             .AsImplementedInterfaces()
             .WithScopedLifetime()
             );

            services.AddCommandR();

            services.AddDistributedRedisCache(x => x.Configuration = configuration.GetValue<string>("CacheConnection"));

            return services;
        }
    }
}

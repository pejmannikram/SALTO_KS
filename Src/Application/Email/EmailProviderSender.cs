﻿using Application.Abstraction.Email;
using System.Threading.Tasks;

namespace Application.Email
{
    public class EmailProviderSender
    {
        private readonly IEmailService email;
        private readonly EmailContent content;

        public EmailProviderSender(IEmailService email, EmailContent content)
        {
            this.email = email;
            this.content = content;
        }

        public async Task Send(string to)
        {
            await email.Send(to, content);
        }
    }
}

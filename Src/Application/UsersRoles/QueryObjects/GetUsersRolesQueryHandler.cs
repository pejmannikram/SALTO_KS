﻿using Application.Abstraction.Common;
using Application.Abstraction.UsersRoles.QueryObjects;
using Domain.Entities;
using EFQueryObject;
using System.Linq;
using System.Threading.Tasks;

namespace Application.UsersRoles.QueryObjects
{
    class GetUsersRolesQueryHandler : IQueryObjectHandler<GetUsersRolesQuery, UserRole>
    {
        private readonly IAPIDbContext context;

        public GetUsersRolesQueryHandler(IAPIDbContext context)
        {
            this.context = context;
        }

        public ValueTask<IQueryable<UserRole>> Handle(GetUsersRolesQuery request)
        {
            var query = context.UserRoles.Where(x => x.UserId == request.UserId);
            return ValueTask.FromResult(query);
        }
    }
}

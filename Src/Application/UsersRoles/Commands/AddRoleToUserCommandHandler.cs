﻿using Application.Abstraction.Common;
using Application.Abstraction.Users.QueryObjects;
using Application.Abstraction.UsersRoles.Commands;
using Application.Abstraction.UsersRoles.QueryObjects;
using Application.Common;
using CommandR;
using Domain.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace Application.UsersRoles.Commands
{
    class AddRoleToUserCommandHandler : ICommandHandler<AddRoleToUserCommand, UserRole>
    {
        private readonly IMediator mediator;
        private readonly IAPIDbContext context;

        public AddRoleToUserCommandHandler(IMediator mediator, IAPIDbContext context)
        {
            this.mediator = mediator;
            this.context = context;
        }

        public async ValueTask<UserRole> Handle(AddRoleToUserCommand request)
        {
            var userQuery = await mediator.Send(new GetUserQuery());
            await Guard.Argument(nameof(User)).Exist(userQuery.Where(x => x.Id == request.UserId));

            var query = await mediator.Send(new GetUsersRolesQuery(request.UserId));
            await Guard.Argument(nameof(UserRole))
                .NotExist(query.Where(x => x.RoleId == request.RoleId));

            var userRole = new UserRole(request.RoleId, request.UserId);
            await context.UserRoles.AddAsync(userRole);

            return userRole;
        }
    }
}

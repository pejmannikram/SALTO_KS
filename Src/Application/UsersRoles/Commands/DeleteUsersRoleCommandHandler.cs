﻿using Application.Abstraction.Common;
using Application.Abstraction.UsersRoles.Commands;
using Application.Abstraction.UsersRoles.QueryObjects;
using Application.Common;
using CommandR;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Application.UsersRoles.Commands
{
    class DeleteUsersRoleCommandHandler : ICommandHandler<DeleteUsersRoleCommand, UserRole>
    {
        private readonly IMediator mediator;
        private readonly IAPIDbContext context;

        public DeleteUsersRoleCommandHandler(IMediator mediator, IAPIDbContext context)
        {
            this.mediator = mediator;
            this.context = context;
        }

        public async ValueTask<UserRole> Handle(DeleteUsersRoleCommand request)
        {
            var query = await mediator.Send(new GetUsersRolesQuery(request.UserId));

            if (!await query.AnyAsync(x => x.RoleId != request.RoleId))
            {
                throw new AppException(x => x.UserNeedsOneRoleAtLeast(), HttpStatusCode.Forbidden);
            }

            var userRole = await query
                .Where(x => x.RoleId == request.RoleId)
                .FirstOrDefaultAsync();

            Guard.Argument(nameof(UserRole)).NotNull(userRole);

            context.UserRoles.Remove(userRole);
            return userRole;
        }
    }
}

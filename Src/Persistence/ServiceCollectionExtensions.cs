﻿using Application.Abstraction.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Persistence
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddPersistence(this IServiceCollection services)
        {
            services.AddDbContextPool<IAPIDbContext, APIDbContext>((serviceProvider, options) =>
            {
                var configuration = serviceProvider.GetRequiredService<IConfiguration>();
                string connection = configuration.GetConnectionString("DefaultConnection");

                options.UseNpgsql(connection)
                  .UseLoggerFactory(LoggerFactory.Create(builder => builder.AddConsole()));
            });

            return services;
        }
    }
}

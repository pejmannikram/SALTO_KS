﻿using Application.Abstraction.Common;
using Domain.Common;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Persistence
{
    public class APIDbContext : DbContext, IAPIDbContext
    {
        public APIDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; } = null!;

        public DbSet<Door> Doors { get; set; } = null!;

        public DbSet<UserDoor> UserDoors { get; set; } = null!;

        public DbSet<DoorAccessHistory> DoorAccessHistories { get; set; } = null!;

        public DbSet<UserRole> UserRoles { get; set; } = null!;
        public DbSet<RefreshToken> RefreshTokens { get; set; } = null!;

        public Task<int> ExecuteSqlRaw(string sql, params object[] parameters)
        {
            if (Database.IsRelational())
            {
                return Database.ExecuteSqlRawAsync(sql, parameters);
            }

            return Task.FromResult(0);
        }

        public Task<int> ExecuteSqlRaw(string sql)
        {
            if (Database.IsRelational())
            {
                return Database.ExecuteSqlRawAsync(sql);
            }

            return Task.FromResult(0);
        }

        public Task<IDbContextTransaction> BeginTransaction()
        {
            return Database.BeginTransactionAsync();
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            AutoFillDateTimeFields();
            return base.SaveChangesAsync(cancellationToken);
        }

        private void AutoFillDateTimeFields()
        {
            var entities = ChangeTracker.Entries<BaseDomain>()
                .Where(x => x.State == EntityState.Added);

            var now = DateTime.UtcNow;

            foreach (var entity in entities)
            {
                entity.Entity.CreatedDate = now;
            }
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(GetType().Assembly);
        }
    }
}

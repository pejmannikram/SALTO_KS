﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Configurations
{
    class UserDoorConfiguration : AbstractConfiguration<UserDoor>
    {
        public override void Config(EntityTypeBuilder<UserDoor> builder)
        {
            builder.HasIndex(x => new { x.DoorId, x.UserId }).IsUnique();
        }
    }
}

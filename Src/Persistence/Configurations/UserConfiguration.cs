﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Configurations
{
    class UserConfiguration : AbstractConfiguration<User>
    {
        public override void Config(EntityTypeBuilder<User> builder)
        {
            builder.HasIndex(x => x.Email).IsUnique();
        }

        public override void Seed(EntityTypeBuilder<User> builder)
        {
            // password = S@c!etP@$$0rd
            builder.HasData(new User(1, "Admin", "", "admin@api.com", "AQAAAAEAACcQAAAAEDrxxsAFV0LvoTHrGZxxkJhSkgJcgykWOHaWZfVO1Pq1vJD56NeaZPu2O4QMhQpefw=="));
        }
    }
}

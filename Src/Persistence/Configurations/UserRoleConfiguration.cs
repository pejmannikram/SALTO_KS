﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Persistence.Configurations
{
    class UserRoleConfiguration : AbstractConfiguration<UserRole>
    {
        public override void Config(EntityTypeBuilder<UserRole> builder)
        {
            builder.HasIndex(x => new { x.UserId, x.RoleId }).IsUnique();
        }

        public override void Seed(EntityTypeBuilder<UserRole> builder)
        {
            builder.HasData(new UserRole(1, Domain.Enums.UserRoleType.Admin, 1));
        }
    }
}

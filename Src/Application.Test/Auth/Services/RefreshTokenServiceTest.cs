﻿using Application.Abstraction.Auth.Models;
using Application.Auth.Services;
using Application.Common;
using Application.Test.Common;
using Domain.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Moq;
using Persistence;
using System;
using System.Linq;
using Xunit;

namespace Application.UnitTest.Auth.Services
{
    public class RefreshTokenServiceTest : IDisposable
    {
        private readonly IRefreshTokenService service;
        private readonly APIDbContext context;
        private readonly Mock<IDateTime> dateTime;
        private readonly Mock<IPasswordHasher<RefreshToken>> passwordHasher;

        public RefreshTokenServiceTest()
        {
            context = APIDbContextFactory.Create();
            dateTime = new Mock<IDateTime>();
            passwordHasher = new Mock<IPasswordHasher<RefreshToken>>();

            dateTime.Setup(x => x.Now).Returns(System.DateTime.Now);

            service = new RefreshTokenService(passwordHasher.Object, dateTime.Object, context);
        }

        [Fact]
        public async void RefreshToken_Success()
        {
            var result = await service.PersistRefreshToken(1, new AuthTokenVM("x", "y", System.DateTime.Now));
            await context.SaveChangesAsync();

            var refreshToken = await context.RefreshTokens
                .Where(x => x.UserId == 1)
                .FirstOrDefaultAsync();

            Assert.NotNull(refreshToken);
            Assert.StartsWith(refreshToken.Id + "_", result.RefreshToken);
            passwordHasher.Verify(x => x.HashPassword(It.IsAny<RefreshToken>(), "y"));
        }

        public void Dispose()
        {
            context.Destroy();
        }
    }
}

﻿using Application.Abstraction.Auth.Commands;
using Application.Abstraction.Auth.Models;
using Application.Abstraction.Common;
using Application.Auth.Commands;
using Application.Auth.Services;
using Application.Common;
using Application.Test.Common;
using CommandR;
using Domain.Entities;
using Domain.Enums;
using EFQueryObject;
using Microsoft.AspNetCore.Identity;
using Moq;
using Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Application.UnitTest.Auth.Commands
{
    public class RefreshAuthTokenTest : IDisposable
    {
        private readonly RefreshAuthTokenCommandHandler handler;
        private readonly Mock<ITokenGeneratorService> tokenGenerator;
        private readonly Mock<IDateTime> dateTime;
        private readonly APIDbContext context;
        private readonly Mock<IPasswordHasher<RefreshToken>> passwordHasher;
        private readonly Mock<IMediator> mediator;

        public RefreshAuthTokenTest()
        {
            context = APIDbContextFactory.Create();
            passwordHasher = new Mock<IPasswordHasher<RefreshToken>>();
            mediator = new Mock<IMediator>();
            tokenGenerator = new Mock<ITokenGeneratorService>();
            dateTime = new Mock<IDateTime>();

            dateTime.Setup(x => x.Now).Returns(System.DateTime.Now);

            mediator.Setup(x => x.Send(It.IsAny<IQueryObject<RefreshToken>>()))
                .ReturnsAsync(context.RefreshTokens);

            mediator.Setup(x => x.Send(It.IsAny<IQueryObject<UserRole>>()))
                .ReturnsAsync(context.UserRoles);

            passwordHasher.Setup(x => x.VerifyHashedPassword(It.IsAny<RefreshToken>(),
                It.IsAny<string>(), It.IsAny<string>()))
                .Returns(PasswordVerificationResult.Success);

            var refreshToken = new RefreshToken(1);
            refreshToken.UpdateToken("xx", System.DateTime.Now);
            context.RefreshTokens.Add(refreshToken);
            context.SaveChanges();

            handler = new RefreshAuthTokenCommandHandler(
              mediator.Object, dateTime.Object,
              tokenGenerator.Object, passwordHasher.Object);
        }

        [Fact]
        public async void RefreshToken_WrongRefreshTokenId_ReturnsException()
        {
            mediator.Setup(x => x.Send(It.IsAny<IQueryObject<RefreshToken>>()))
               .ReturnsAsync(context.RefreshTokens.Where(x => false));

            var command = new RefreshAuthTokenCommand("2_xx");
            await Assert.ThrowsAsync<AppException>(async () => await handler.Handle(command));
        }

        [Fact]
        public async void RefreshToken_WrongRefreshToken_ReturnsException()
        {
            passwordHasher.Setup(x => x.VerifyHashedPassword(It.IsAny<RefreshToken>(),
               It.IsAny<string>(), It.IsAny<string>()))
               .Returns(PasswordVerificationResult.Failed);

            var command = new RefreshAuthTokenCommand("1_yy");
            await Assert.ThrowsAsync<AppException>(async () => await handler.Handle(command));
        }

        [Fact]
        public async void RefreshToken_ExpiredToken_ReturnsException()
        {
            dateTime.Setup(x => x.Now).Returns(System.DateTime.Now.AddDays(30));

            var command = new RefreshAuthTokenCommand("1_xx");
            await Assert.ThrowsAsync<AppException>(async () => await handler.Handle(command));
        }

        [Fact]
        public async void RefreshToken_Success_ReturnsAuthToken()
        {
            tokenGenerator.Setup(x => x.GenerateAuthToken(1, It.IsAny<IEnumerable<UserRoleType>>()))
                .Returns(new AuthTokenVM("x", "y", System.DateTime.Now));

            var command = new RefreshAuthTokenCommand("1_xx");
            var result = await handler.Handle(command);

            Assert.IsType<AuthTokenVM>(result);
        }

        public void Dispose()
        {
            context.Destroy();
        }
    }
}

﻿using Application.Abstraction.Auth.Commands;
using Application.Abstraction.Common;
using Application.Auth.Commands;
using Application.Test.Common;
using CommandR;
using Domain.Entities;
using EFQueryObject;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Moq;
using Persistence;
using System;
using System.Linq;
using Xunit;

namespace Application.UnitTest.Auth.Commands
{
    public class RevokeRefreshTokenCommandTest : IDisposable
    {
        private readonly RevokeRefreshTokenCommandHandler handler;
        private readonly Mock<IUserService> userService;
        private readonly APIDbContext context;
        private readonly Mock<IPasswordHasher<RefreshToken>> passwordHasher;
        private readonly Mock<IMediator> mediator;

        public RevokeRefreshTokenCommandTest()
        {
            context = APIDbContextFactory.Create();
            passwordHasher = new Mock<IPasswordHasher<RefreshToken>>();
            mediator = new Mock<IMediator>();
            userService = new Mock<IUserService>();

            mediator.Setup(x => x.Send(It.IsAny<IQueryObject<RefreshToken>>()))
                .ReturnsAsync(context.RefreshTokens);

            passwordHasher.Setup(x => x.VerifyHashedPassword(It.IsAny<RefreshToken>(),
                It.IsAny<string>(), It.IsAny<string>()))
                .Returns(PasswordVerificationResult.Success);

            userService.Setup(x => x.UserId)
                .Returns(1);

            var refreshToken = new RefreshToken(1);
            refreshToken.UpdateToken("xx", System.DateTime.Now);
            context.RefreshTokens.Add(refreshToken);
            context.SaveChanges();

            handler = new RevokeRefreshTokenCommandHandler(mediator.Object,
                context, passwordHasher.Object, userService.Object);
        }

        [Fact]
        public async void RevokeRefreshToken_WrongRefreshTokenId_ReturnsVoid()
        {
            mediator.Setup(x => x.Send(It.IsAny<IQueryObject<RefreshToken>>()))
               .ReturnsAsync(context.RefreshTokens.Where(x => false));

            var command = new RevokeRefreshTokenCommand("2_xx");
            var result = await handler.Handle(command);
            await context.SaveChangesAsync();

            Assert.IsType<VoidResult>(result);

            var refreshTokensCount = await context.RefreshTokens.CountAsync();
            Assert.Equal(1, refreshTokensCount);
        }

        [Fact]
        public async void RevokeRefreshToken_WrongRefreshToken_ReturnsVoid()
        {
            passwordHasher.Setup(x => x.VerifyHashedPassword(It.IsAny<RefreshToken>(),
               It.IsAny<string>(), It.IsAny<string>()))
               .Returns(PasswordVerificationResult.Failed);

            var command = new RevokeRefreshTokenCommand("1_yy");
            var result = await handler.Handle(command);
            await context.SaveChangesAsync();

            Assert.IsType<VoidResult>(result);

            var refreshTokensCount = await context.RefreshTokens.CountAsync();
            Assert.Equal(1, refreshTokensCount);
        }

        [Fact]
        public async void RevokeRefreshToken_Success_RemoveTheToken()
        {
            var command = new RevokeRefreshTokenCommand("1_yy");
            var result = await handler.Handle(command);
            await context.SaveChangesAsync();

            Assert.IsType<VoidResult>(result);

            var refreshTokensCount = await context.RefreshTokens.CountAsync();
            Assert.Equal(0, refreshTokensCount);
        }

        public void Dispose()
        {
            context.Destroy();
        }
    }
}

﻿using Application.Abstraction.Auth.Commands;
using Application.Abstraction.Auth.Models;
using Application.Abstraction.Common;
using Application.Auth.Commands;
using Application.Auth.Services;
using Application.Test.Common;
using CommandR;
using Domain.Entities;
using EFQueryObject;
using Microsoft.AspNetCore.Identity;
using Moq;
using Persistence;
using System;
using Xunit;

namespace Application.Test.Auth.Commands
{
    public class LoginCommandTest : IDisposable
    {
        private readonly LoginCommandHandler handler;
        private readonly APIDbContext context;
        private readonly Mock<IRefreshTokenService> refreshToken;
        private readonly Mock<IMediator> mediator;
        private readonly Mock<IPasswordHasher<User>> passwordHasher;
        private readonly Mock<ITokenGeneratorService> tokenGenerator;

        public LoginCommandTest()
        {
            mediator = new Mock<IMediator>();
            passwordHasher = new Mock<IPasswordHasher<User>>();
            tokenGenerator = new Mock<ITokenGeneratorService>();
            refreshToken = new Mock<IRefreshTokenService>();
            context = APIDbContextFactory.Create();

            mediator.Setup(x => x.Send(It.IsAny<IQueryObject<User>>()))
                .ReturnsAsync(context.Users);

            passwordHasher.Setup(x => x.VerifyHashedPassword(It.IsAny<User>(),
                It.IsAny<string>(), It.IsAny<string>()))
                .Returns(PasswordVerificationResult.Success);

            handler = new LoginCommandHandler(
              mediator.Object, refreshToken.Object,
              passwordHasher.Object, tokenGenerator.Object);
        }

        [Fact]
        public async void Login_WrongEmail_ReturnsException()
        {
            var command = new LoginCommand("wrong@api.com", "123");
            await Assert.ThrowsAsync<AppException>(async () => await handler.Handle(command));
        }

        [Fact]
        public async void Login_WrongPassword_ReturnsException()
        {
            passwordHasher.Setup(x => x.VerifyHashedPassword(It.IsAny<User>(),
                It.IsAny<string>(), It.IsAny<string>()))
                .Returns(PasswordVerificationResult.Failed);

            var command = new LoginCommand("admin@api.com", "123");

            await Assert.ThrowsAsync<AppException>(async () => await handler.Handle(command));
        }

        [Fact]
        public async void Login_Success_ReturnToken()
        {
            var command = new LoginCommand("admin@api.com", "S@c!etP@$$0rd");
            var result = await handler.Handle(command);

            refreshToken.Verify(x => x.PersistRefreshToken(1, It.IsAny<AuthTokenVM>()));
        }

        public void Dispose()
        {
            context.Destroy();
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Persistence;
using System;

namespace Application.Test.Common
{
    static class APIDbContextFactory
    {
        public static APIDbContext Create()
        {
            var options = new DbContextOptionsBuilder<APIDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            var context = new APIDbContext(options);

            context.Database.EnsureCreated();

            context.SaveChanges();

            return context;
        }

        public static void Destroy(this APIDbContext context)
        {
            context.Database.EnsureDeleted();
            context.Dispose();
        }
    }
}

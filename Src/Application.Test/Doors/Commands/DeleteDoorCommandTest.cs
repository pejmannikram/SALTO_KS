﻿using Application.Abstraction.Common;
using Application.Abstraction.Doors.Commands;
using Application.Doors.Commands;
using Application.Test.Common;
using CommandR;
using Domain.Entities;
using EFQueryObject;
using Microsoft.EntityFrameworkCore;
using Moq;
using Persistence;
using System;
using Xunit;

namespace Application.UnitTest.Doors.Commands
{
    public class DeleteDoorCommandTest : IDisposable
    {
        private readonly APIDbContext context;
        private readonly Mock<IMediator> mediator;
        private readonly DeleteDoorCommandHandler handler;

        public DeleteDoorCommandTest()
        {
            context = APIDbContextFactory.Create();
            mediator = new Mock<IMediator>();

            context.Doors.Add(new Door("x", "y"));
            context.SaveChanges();

            mediator.Setup(x => x.Send(It.IsAny<IQueryObject<Door>>()))
               .ReturnsAsync(context.Doors);

            handler = new DeleteDoorCommandHandler(context, mediator.Object);
        }

        [Fact]
        public async void DeleteDoor_WrongId_ReturnsException()
        {
            var command = new DeleteDoorCommand(2);
            await Assert.ThrowsAsync<AppException>(async () => await handler.Handle(command));
        }

        [Fact]
        public async void DeleteDoor_Success()
        {
            var command = new DeleteDoorCommand(1);
            await handler.Handle(command);
            await context.SaveChangesAsync();

            var doorExist = await context.Doors.AnyAsync();

            Assert.False(doorExist);
        }

        public void Dispose()
        {
            context.Destroy();
        }
    }
}

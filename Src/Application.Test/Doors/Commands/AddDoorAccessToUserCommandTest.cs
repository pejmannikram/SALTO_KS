﻿using Application.Abstraction.Common;
using Application.Abstraction.Doors.Commands;
using Application.Doors.Commands;
using Application.Test.Common;
using CommandR;
using Domain.Entities;
using EFQueryObject;
using Microsoft.EntityFrameworkCore;
using Moq;
using Persistence;
using System;
using System.Linq;
using Xunit;

namespace Application.UnitTest.Doors.Commands
{
    public class AddDoorAccessToUserCommandTest : IDisposable
    {
        private readonly APIDbContext context;
        private readonly Mock<IMediator> mediator;
        private readonly AddDoorAccessToUserCommandHandler handler;

        public AddDoorAccessToUserCommandTest()
        {
            context = APIDbContextFactory.Create();
            mediator = new Mock<IMediator>();

            context.Doors.Add(new Door("x", "y"));
            context.SaveChanges();

            mediator.Setup(x => x.Send(It.IsAny<IQueryObject<User>>()))
                .ReturnsAsync(context.Users);

            mediator.Setup(x => x.Send(It.IsAny<IQueryObject<Door>>()))
               .ReturnsAsync(context.Doors);

            mediator.Setup(x => x.Send(It.IsAny<IQueryObject<UserDoor>>()))
               .ReturnsAsync(context.UserDoors);


            handler = new AddDoorAccessToUserCommandHandler(mediator.Object, context);
        }

        [Fact]
        public async void AddDoorAccess_WrongUserId_ReturnsException()
        {
            var command = new AddDoorAccessToUserCommand(2, 1);
            await Assert.ThrowsAsync<AppException>(async () => await handler.Handle(command));
        }

        [Fact]
        public async void AddDoorAccess_WrongDoorId_ReturnsException()
        {
            var command = new AddDoorAccessToUserCommand(1, 2);
            await Assert.ThrowsAsync<AppException>(async () => await handler.Handle(command));
        }

        [Fact]
        public async void AddDoorAccess_Duplicate_ReturnsException()
        {
            var command = new AddDoorAccessToUserCommand(1, 1);
            await handler.Handle(command);
            await context.SaveChangesAsync();

            await Assert.ThrowsAsync<AppException>(async () => await handler.Handle(command));
        }

        [Fact]
        public async void AddDoorAccess_Success()
        {
            var command = new AddDoorAccessToUserCommand(1, 1);
            await handler.Handle(command);
            await context.SaveChangesAsync();

            var userDoorExist = await context.UserDoors
                 .Where(x => x.DoorId == 1)
                 .Where(x => x.UserId == 1)
                 .AnyAsync();

            Assert.True(userDoorExist);
        }


        public void Dispose()
        {
            context.Destroy();
        }
    }
}

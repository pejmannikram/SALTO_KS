﻿using Application.Abstraction.Common;
using Application.Abstraction.Doors.Commands;
using Application.Doors.Commands;
using Application.Test.Common;
using CommandR;
using Domain.Entities;
using EFQueryObject;
using Microsoft.EntityFrameworkCore;
using Moq;
using Persistence;
using System;
using System.Linq;
using Xunit;

namespace Application.UnitTest.Doors.Commands
{
    public class CreateDoorCommandTest : IDisposable
    {
        private readonly APIDbContext context;
        private readonly Mock<IMediator> mediator;
        private readonly CreateDoorCommandHandler handler;

        public CreateDoorCommandTest()
        {
            context = APIDbContextFactory.Create();
            mediator = new Mock<IMediator>();

            context.Doors.Add(new Door("x", "y"));
            context.SaveChanges();

            mediator.Setup(x => x.Send(It.IsAny<IQueryObject<Door>>()))
               .ReturnsAsync(context.Doors);

            handler = new CreateDoorCommandHandler(mediator.Object, context);
        }

        [Fact]
        public async void CreateDoor_DuplicateName_ReturnsException()
        {
            var command = new CreateDoorCommand("x", "y");
            await Assert.ThrowsAsync<AppException>(async () => await handler.Handle(command));
        }

        [Fact]
        public async void CreateDoor_Success()
        {
            var command = new CreateDoorCommand("x2", "y2");
            await handler.Handle(command);
            await context.SaveChangesAsync();

            var doorExist = await context.Doors
                 .Where(x => x.Name == "x2")
                 .AnyAsync();

            Assert.True(doorExist);
        }


        public void Dispose()
        {
            context.Destroy();
        }
    }
}

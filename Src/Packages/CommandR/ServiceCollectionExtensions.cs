﻿using CommandR;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using System.Reflection;

namespace EFQueryObject
{
    public static class CommandR
    {
        public static IServiceCollection AddCommandR(this IServiceCollection services)
        {

            var assembly = Assembly.GetCallingAssembly();

            services.Scan(scan => scan
             .FromAssemblies(assembly)
             .AddClasses(classes => classes.AssignableTo(typeof(ICommandPipeline<,>)))
             .AsImplementedInterfaces()
             .WithScopedLifetime()

             .AddClasses(classes => classes.AssignableTo<IBaseCommandHandler>())
             .As(type => type
                 .GetInterfaces()
                 .Where(x =>
                      x.IsGenericType &&
                      x.GetGenericTypeDefinition() == typeof(ICommandHandler<,>))
              )
             .WithScopedLifetime()
            );


            services.AddScoped(typeof(ICommandHandlerWrapper<,>), typeof(CommandHandlerWrapper<,>));
            services.AddScoped<IMediator, Mediator>();

            return services;
        }
    }
}

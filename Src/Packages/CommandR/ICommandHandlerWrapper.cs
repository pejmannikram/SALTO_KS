﻿using System.Threading.Tasks;

namespace CommandR
{
    public interface ICommandHandlerWrapper<in TRequest, TResponse> : IBaseCommandHandler where TRequest : ICommand<TResponse>
    {
        ValueTask<TResponse> Handle(TRequest request);
    }
}

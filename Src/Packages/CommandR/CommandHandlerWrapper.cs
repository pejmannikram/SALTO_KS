﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommandR
{
    public class CommandHandlerWrapper<TRequest, TResponse> : ICommandHandlerWrapper<TRequest, TResponse> where TRequest : ICommand<TResponse>
    {
        private readonly ICommandHandler<TRequest, TResponse> commandHandler;
        private readonly IEnumerable<ICommandPipeline<TRequest, TResponse>> pipelines;
        private bool isNested = false;

        public CommandHandlerWrapper(
            ICommandHandler<TRequest, TResponse> commandHandler,
            IEnumerable<ICommandPipeline<TRequest, TResponse>> pipelines)
        {
            this.commandHandler = commandHandler;
            this.pipelines = pipelines;
        }

        public async ValueTask<TResponse> Handle(TRequest request)
        {
            /// in nested commands call the pipeline will be not called.
            /// P1 →→ P2 →→ handler1 ↓
            ///             handler2 ↓
            /// P1 ←← P2 ←← ←--------
            if (isNested)
            {
                return await commandHandler.Handle(request);
            }

            /// we are in the middle of running a command
            isNested = true;

            PipelineDelegate<TResponse> HandlerFnc = () =>
            {
                var result = commandHandler.Handle(request);
                isNested = false;
                return result;
            };

            var pipelineFnc = pipelines
                .Reverse()
                .Aggregate(HandlerFnc, (next, current) => () => current.Handle(request, next));

            return await pipelineFnc();
        }
    }
}

﻿using System.Threading.Tasks;

namespace CommandR
{
    public interface IMediator
    {
        /// <summary>
        /// Send a command object to handler and get response
        /// </summary>
        /// <typeparam name="TResponse">the result entity</typeparam>
        /// <param name="command">command object</param>
        /// <returns></returns>
        ValueTask<TResponse> Send<TResponse>(ICommand<TResponse> command);
    }
}

﻿using System.Threading.Tasks;

namespace CommandR
{
    public interface IBaseCommandHandler { }
    public interface ICommandHandler<in TRequest, TResponse> : IBaseCommandHandler where TRequest : ICommand<TResponse>
    {
        ValueTask<TResponse> Handle(TRequest request);
    }
}

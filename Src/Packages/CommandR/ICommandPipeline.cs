﻿using System.Threading.Tasks;

namespace CommandR
{
    public delegate ValueTask<TResponse> PipelineDelegate<TResponse>();

    public interface ICommandPipeline<in TRequest, TResponse>
    {
        ValueTask<TResponse> Handle(TRequest request, PipelineDelegate<TResponse> next);
    }
}

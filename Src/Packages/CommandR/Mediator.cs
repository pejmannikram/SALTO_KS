﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CommandR
{
    public class Mediator : IMediator
    {
        private readonly IServiceProvider serviceProvider;

        public Mediator(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        ///<inheritdoc/>
        public ValueTask<TResponse> Send<TResponse>(ICommand<TResponse> command)
        {
            var requestType = command.GetType();
            var serviceType = typeof(ICommandHandlerWrapper<,>).MakeGenericType(requestType, typeof(TResponse));
            var handler = serviceProvider.GetRequiredService(serviceType);
            var handlerType = handler.GetType().GetMethods().First();

            var result = handlerType.Invoke(handler, new object[] { command });
            if (result is ValueTask<TResponse> taskResult)
            {
                return taskResult;
            }

            throw new ArgumentException("The command handler returns null instead of a TResponse", nameof(command));
        }
    }
}

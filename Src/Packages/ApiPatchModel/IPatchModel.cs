﻿using System;

namespace ApiPatchModel
{
    public interface IPatchModel
    {
        public Type ModelType { get; }
        public object GetModel();

        bool HasProperty(string propertyName);
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace ApiPatchModel
{
    [ModelBinder(BinderType = typeof(PatchModelBinder))]
    public class PatchModel<T> : IPatchModel where T : class
    {
        private readonly IDictionary<string, object?> values;
        private readonly PropertyInfo[] propertiesInfo;

        public Type ModelType => typeof(T);

        public PatchModel(Dictionary<string, object?> values)
        {
            this.values = new Dictionary<string, object?>(values, StringComparer.OrdinalIgnoreCase);
            propertiesInfo = GetProperties(typeof(T));
        }

        private PropertyInfo[] GetProperties(Type type)
        {
            return type.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.SetProperty);
        }

        public bool HasProperty(string propertyName)
        {
            return values.ContainsKey(propertyName);
        }

        public bool IsChanged<TProperty>(Expression<Func<T, TProperty>> expression, TProperty modelValue)
        {
            if (TryGetValue(expression, out var value))
            {
                if (value is null)
                {
                    return modelValue is null;
                }
                return !value.Equals(modelValue);
            }

            return false;
        }

        public bool TryGetValue<TProperty>(Expression<Func<T, TProperty>> expression, [NotNullWhen(true)] out TProperty? value)
        {
            if (expression.Body is not MemberExpression body)
            {
                body = (MemberExpression)((UnaryExpression)expression.Body).Operand;
            }

            if (values.TryGetValue(body.Member.Name, out var rawValue) && rawValue != null)
            {
                value = (TProperty)ResolveFieldValueType(rawValue, typeof(TProperty))!;
                return true;
            }

            value = default;
            return false;
        }

        public T GetModel()
        {
            var model = (T)Activator.CreateInstance(typeof(T), true)!;
            MapTo(model);
            return model;
        }

        private object? ResolveFieldValueType(object? value, Type propertyType)
        {
            if (value != null && value.GetType() != propertyType)
            {
                var nullableType = Nullable.GetUnderlyingType(propertyType);

                if (nullableType != null)
                {
                    propertyType = nullableType;
                }

                return propertyType.IsEnum ? Enum.Parse(propertyType, (string)value, true) : Convert.ChangeType(value, propertyType);
            }

            return value;
        }

        public void MapTo<TDomain>(TDomain domain)
        {
            var domainProperties = typeof(TDomain) == typeof(T) ? propertiesInfo : GetProperties(typeof(TDomain));

            foreach (var (property, value) in values)
            {
                var domainProperty = domainProperties.Where(x => x.Name.Equals(property, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();

                if (domainProperty != null)
                {
                    var domainValue = domainProperty.GetMethod?.Invoke(domain, Array.Empty<object>());
                    if (value != domainValue && (value is null || domainValue is null || !value.Equals(domainValue)))
                    {
                        var propertyValue = ResolveFieldValueType(value, domainProperty.PropertyType);
                        domainProperty.SetMethod?.Invoke(domain, new object?[] { propertyValue });
                    }
                }
            }
        }

        object IPatchModel.GetModel()
        {
            return GetModel();
        }
    }
}

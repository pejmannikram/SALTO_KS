﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace ApiPatchModel
{
    public class PatchModelBinder : IModelBinder
    {
        public async Task BindModelAsync(ModelBindingContext bindingContext)
        {
            if (bindingContext == null)
            {
                throw new ArgumentNullException(nameof(bindingContext));
            }

            var body = bindingContext.HttpContext.Request.Body;
            using var reader = new StreamReader(body);
            var content = await reader.ReadToEndAsync();
            var inputData = JsonSerializer.Deserialize<Dictionary<string, JsonElement>>(content, new JsonSerializerOptions { });

            if (inputData is not null)
            {
                var constructor = bindingContext.ModelType.GetConstructors().First();
                var model = constructor.Invoke(new[] { ParseInput(inputData) });

                bindingContext.Result = ModelBindingResult.Success(model);
            }
            else
            {
                bindingContext.Result = ModelBindingResult.Failed();
            }
        }

        private object ParseInput(Dictionary<string, JsonElement> inputData)
        {
            var result = new Dictionary<string, object?>();
            foreach (var (key, value) in inputData)
            {
                result.Add(key, ParseJsonElement(value));
            }

            return result;
        }

        private object? ParseJsonElement(JsonElement element)
        {
            object? result = null;

            switch (element.ValueKind)
            {
                case JsonValueKind.Null:
                    result = null;
                    break;
                case JsonValueKind.Number:
                    result = element.GetDouble();
                    break;
                case JsonValueKind.False:
                    result = false;
                    break;
                case JsonValueKind.True:
                    result = true;
                    break;
                case JsonValueKind.Undefined:
                    result = null;
                    break;
                case JsonValueKind.String:
                    result = element.GetString();
                    break;
                case JsonValueKind.Object:
                    result = ParseJsonElement(element);
                    break;
                case JsonValueKind.Array:
                    result = element.EnumerateArray()
                        .Select(x => ParseJsonElement(x))
                        .ToArray();
                    break;
            }

            return result;
        }
    }
}

﻿using CommandR;
using System.Linq;

namespace EFQueryObject
{
    public interface IQueryObjectHandler<TRequest, TResponse> : ICommandHandler<TRequest, IQueryable<TResponse>> where TRequest : IQueryObject<TResponse>
    {

    }
}

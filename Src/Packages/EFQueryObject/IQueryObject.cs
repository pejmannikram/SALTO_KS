﻿using CommandR;
using System.Linq;

namespace EFQueryObject
{

    public interface IQueryObject<TDomain> : ICommand<IQueryable<TDomain>>
    {

    }
}

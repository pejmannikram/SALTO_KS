﻿using Application.Abstraction.DoorClient;
using Application.Abstraction.Email;
using Infrastructure.DoorCommand;
using Infrastructure.Email;
using Microsoft.Extensions.DependencyInjection;

namespace Persistence
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services)
        {
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<IDoorClientService, DoorClientService>();

            return services;
        }
    }
}

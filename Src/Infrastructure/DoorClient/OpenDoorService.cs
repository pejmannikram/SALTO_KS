﻿using Application.Abstraction.DoorClient;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Infrastructure.DoorCommand
{
    public class DoorClientService : IDoorClientService
    {
        private readonly ILogger<DoorClientService> logger;

        public DoorClientService(ILogger<DoorClientService> logger)
        {
            this.logger = logger;
        }

        public Task Open(string doorExternalId)
        {
            //TODO: replace with door client
            logger.LogInformation("--------- \n" +
            $"      Open door: {doorExternalId} \n" +
            "      ---------");

            return Task.CompletedTask;
        }
    }
}

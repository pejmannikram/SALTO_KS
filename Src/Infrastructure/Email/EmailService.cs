﻿using Application.Abstraction.Email;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Infrastructure.Email
{
    class EmailService : IEmailService
    {
        private readonly ILogger<EmailService> logger;

        public EmailService(ILogger<EmailService> logger)
        {
            this.logger = logger;
        }

        public Task Send(string to, EmailContent content)
        {
            //TODO: replace with email provider
            logger.LogInformation("--------- \n" +
            $"      Email to: {to} \n" +
            $"      Parameters : {string.Join(Environment.NewLine, content.Parameters)} \n" +
            "      ---------");

            return Task.CompletedTask;
        }
    }
}

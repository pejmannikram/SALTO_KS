## Requirement

- NET 5.0
- PostgreSQL
- Redis

For development you can use the provided docker-compose to serve these services:

```
docker-compose -f Development.yml up -d
```

---
### List Resource Query String

The List Resources support pagination, sort, and filter over the query string.

- sorts is a comma-delimited ordered list of property names to sort by. Adding a - before the name switches to sort descending.

- filters is a comma-delimited list of {Name}{Operator}{Value} where
{Name} is the name of a property and {Operator} is one of the Operators and {Value} is the value to use for filtering

- page is the number of page to return
-pageSize is the number of items returned per page

#### Operators
| Operator   | Meaning                  |
|------------|--------------------------|
| `==`       | Equals                   |
| `!=`       | Not equals               |
| `>`        | Greater than             |
| `<`        | Less than                |
| `>=`       | Greater than or equal to |
| `<=`       | Less than or equal to    |
| `@=`       | Contains                 |
| `_=`       | Starts with              |
| `!@=`      | Does not Contains        |
| `!_=`      | Does not Starts with     |
| `@=*`      | Case-insensitive string Contains |
| `_=*`      | Case-insensitive string Starts with |
| `==*`      | Case-insensitive string Equals |
| `!=*`      | Case-insensitive string Not equals |
| `!@=*`     | Case-insensitive string does not Contains |
| `!_=*`     | Case-insensitive string does not Starts with |


for more info please visit:
https://github.com/Biarity/Sieve#send-a-request

---
## Migration

In case of needs to run migration you need to provide -p and -s options as follow:

```
dotnet ef migrations add [NAME] -p ./Src/Persistence -s ./Src/Web
```

```
dotnet ef database update -p ./Src/Persistence -s ./Src/Web
```

---
## Docker file

To run the project from docker you can use:

```
docker run \
-e CacheConnection="" \
-e PrivateKey="" \
-e ConnectionStrings__DefaultConnection="" \
-p 5000:80 IMAGE
```


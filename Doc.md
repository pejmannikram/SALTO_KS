# Architecture and Project Decisions

# Database
Relation database is a good fit for this kind of project because we have a static structure with predefined relation and PostgreSQL was Free, open-source, and fast.

# Architecture

I use Clean Architecture to dividing the software into layers in favor of:

- Independent of Frameworks
- Testable
- Independent of UI
- Independent of Database
- Independent of any external agency

This architect avoids the extra abstractions layer and helps me to have a clean and maintainable code.

## Query Object

As I use Entity Framework Core, and this library already implemented Repository pattern I use query object to have a single source for each query over the database. Each object represents a query in compare to add an extra Repository pattern over the EF. they are a simple and small unit that have a single job.

Use Query Object compare to use EF directly let me localized in a single place for resolving database entity. this way if we need to add an extra condition or change the query. we need just update one place.

for instance, we may need an `IsActive` field for doors. and users may not get the inactive doors. we can easily add a where condition based on user role in the related Query Object.

## Command Chain

To fallow single responsibility and keep it decouple, I start the project with a command/mediator patter then for the purpose of handling cross-cutting concern, I add pipeline support to it for handler transaction and save operation.

This pattern can easily be tested and let me hide the implementation of the Application layer.


## DRY

I think I make everything dynamic a bit more than I should, to avoid repeating myself. But it is a trade off over complexity/performance.

## Packages Folder

I start a small package in this project to have the Query Object and Command Chain, but it gets complexity when I finished the assignment. In contrast, in the MediateR, these packages can be called nested and support pipelines.

Also, I implemented a small package to support model patching and RESTful API standard of Patch resources.

I have a plan to publish this package on my GitHub because I really like them :)

# Time Issue
due to time issue I can't complete test. sorry about that.
FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build-env

WORKDIR /app

# Copy everything else and build
COPY . ./

# Copy csproj and restore as distinct layers
WORKDIR /app/
RUN dotnet restore
RUN dotnet publish -c Release -o out

ARG connectionString
ENV ConnectionStrings__DefaultConnection=$connectionString

RUN dotnet tool install --global dotnet-ef --version 5.0.1
# due to bug in sdk image
# https://github.com/dotnet/dotnet-docker/issues/520
ENV PATH="${PATH}:/root/.dotnet/tools"

RUN dotnet ef database update -p ./Src/Persistence -s ./Src/Web

# Build runtime image
FROM mcr.microsoft.com/dotnet/aspnet:5.0

WORKDIR /app
COPY --from=build-env /app/out .

EXPOSE 80

ENTRYPOINT ["dotnet",  "Web.dll"]
